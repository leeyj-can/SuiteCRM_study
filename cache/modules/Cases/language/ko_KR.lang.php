<?php
// created: 2015-12-29 21:02:17
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '등록일:',
  'LBL_DATE_MODIFIED' => '변경일:',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => '상세:',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => 'Created User',
  'LBL_MODIFIED_USER' => 'Modified User',
  'LBL_LIST_NAME' => '이름',
  'LBL_EDIT_BUTTON' => '편집',
  'LBL_REMOVE' => '삭제',
  'LBL_ASSIGNED_TO_ID' => '할당:',
  'LBL_ASSIGNED_TO_NAME' => '할당유저',
  'LBL_NUMBER' => '넘버:',
  'LBL_STATUS' => '상태:',
  'LBL_PRIORITY' => '우선순위:',
  'LBL_RESOLUTION' => '리솔루션:',
  'LBL_LAST_MODIFIED' => '최종변경일',
  'LBL_WORK_LOG' => 'Work Log',
  'LBL_CREATED_BY' => '등록자:',
  'LBL_DATE_CREATED' => '등록일:',
  'LBL_MODIFIED_BY' => '최종편집일:',
  'LBL_ASSIGNED_USER' => 'Assigned User:',
  'LBL_SYSTEM_ID' => '시스템ID',
  'LBL_TYPE' => '종류',
  'LBL_SUBJECT' => '제목:',
  'ERR_DELETE_RECORD' => 'You must specify a record number to delete the account.',
  'LBL_TOOL_TIP_BOX_TITLE' => 'KnowledgeBase Suggestions',
  'LBL_TOOL_TIP_TITLE' => 'Title: ',
  'LBL_TOOL_TIP_BODY' => 'Body: ',
  'LBL_TOOL_TIP_INFO' => 'Additional Info: ',
  'LBL_TOOL_TIP_USE' => 'Use as: ',
  'LBL_SUGGESTION_BOX' => 'Suggestions',
  'LBL_NO_SUGGESTIONS' => 'No Suggestions',
  'LBL_RESOLUTION_BUTTON' => '리솔루션',
  'LBL_SUGGESTION_BOX_STATUS' => '상태',
  'LBL_SUGGESTION_BOX_TITLE' => '직위:',
  'LBL_SUGGESTION_BOX_REL' => 'Relevance',
  'LBL_ACCOUNT_ID' => '거래처ID',
  'LBL_ACCOUNT_NAME' => '거래처명:',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => '거래처',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '영업활동',
  'LBL_ATTACH_NOTE' => 'Attach Note',
  'LBL_BUGS_SUBPANEL_TITLE' => '결함정보',
  'LBL_CASE_NUMBER' => '사례넘버:',
  'LBL_CASE_SUBJECT' => '사례제목:',
  'LBL_CASE' => '사례:',
  'LBL_CONTACT_CASE_TITLE' => '거래담당자-사례:',
  'LBL_CONTACT_NAME' => '거래담당자명:',
  'LBL_CONTACT_ROLE' => '역활:',
  'LBL_CONTACTS_SUBPANEL_TITLE' => '거래담당자',
  'LBL_DEFAULT_SUBPANEL_TITLE' => '사례',
  'LBL_FILENANE_ATTACHMENT' => 'File Attachment',
  'LBL_HISTORY_SUBPANEL_TITLE' => '이력',
  'LBL_INVITEE' => '거래담당자',
  'LBL_MEMBER_OF' => '거래처',
  'LBL_MODULE_NAME' => '사례',
  'LBL_MODULE_TITLE' => '사례: 홈',
  'LBL_NEW_FORM_TITLE' => '신규사례작성',
  'LBL_PROJECTS_SUBPANEL_TITLE' => '프로젝트',
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => '자료관리',
  'LBL_SEARCH_FORM_TITLE' => '사례 검색',
  'LBL_LIST_ASSIGNED_TO_NAME' => '할당유저',
  'LBL_LIST_ACCOUNT_NAME' => '거래처명',
  'LBL_LIST_ASSIGNED' => '할당',
  'LBL_LIST_CLOSE' => '종료',
  'LBL_LIST_FORM_TITLE' => '사례 리스트',
  'LBL_LIST_LAST_MODIFIED' => '최종변경일',
  'LBL_LIST_MY_CASES' => 'My 사례 리스트',
  'LBL_LIST_NUMBER' => '번호.',
  'LBL_LIST_PRIORITY' => '우선순위',
  'LBL_LIST_STATUS' => '상태',
  'LBL_LIST_SUBJECT' => '제목',
  'LNK_CASE_LIST' => '사례 리스트',
  'LNK_NEW_CASE' => '신규사례작성',
  'NTC_REMOVE_FROM_BUG_CONFIRMATION' => 'Are you sure you want to remove this case from the bug?',
  'NTC_REMOVE_INVITEE' => 'Are you sure you want to remove this contact from this case?',
  'LBL_LIST_DATE_CREATED' => '등록일',
  'LNK_IMPORT_CASES' => 'Import Cases',
  'LBL_PROJECT_SUBPANEL_TITLE' => '프로젝트',
  'LBL_CASE_INFORMATION' => '리드 정보',
  'LBL_MODIFIED_BY_NAME_OWNER' => 'Modified By Name Owner',
  'LBL_MODIFIED_BY_NAME_MOD' => 'Modified By Name Mod',
  'LBL_CREATED_BY_NAME_OWNER' => 'Created By Name Owner',
  'LBL_CREATED_BY_NAME_MOD' => 'Created By Name Mod',
  'LBL_ASSIGNED_USER_NAME_OWNER' => 'Assigned User Name Owner',
  'LBL_ASSIGNED_USER_NAME_MOD' => 'Assigned User Name Mod',
  'LBL_TEAM_COUNT_OWNER' => 'Team Count Owner',
  'LBL_TEAM_COUNT_MOD' => 'Team Count Mod',
  'LBL_TEAM_NAME_OWNER' => 'Team Name Owner',
  'LBL_TEAM_NAME_MOD' => 'Team Name Mod',
  'LBL_ACCOUNT_NAME_OWNER' => 'Account Name Owner',
  'LBL_ACCOUNT_NAME_MOD' => 'Account Name Mod',
  'LBL_MODIFIED_USER_NAME' => 'Modified User Name',
  'LBL_MODIFIED_USER_NAME_OWNER' => 'Modified User Name Owner',
  'LBL_MODIFIED_USER_NAME_MOD' => 'Modified User Name Mod',
  'LBL_PORTAL_VIEWABLE' => 'Portal Viewable',
  'LBL_EXPORT_ASSIGNED_USER_ID' => 'Assigned User ID',
  'LBL_EXPORT_MODIFIED_USER_ID' => 'Modified By ID',
  'LBL_EXPORT_CREATED_BY' => 'Created By ID',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Created By User Name',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
  'LBL_EXPORT_TEAM_COUNT' => 'Team Count',
  'LBL_CONTACT_HISTORY_SUBPANEL_TITLE' => 'Related Contacts\' Emails',
);