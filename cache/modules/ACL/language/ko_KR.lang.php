<?php
// created: 2016-01-04 09:57:08
$mod_strings = array (
  'LBL_ALLOW_ALL' => '전부',
  'LBL_ALLOW_NONE' => '미사용',
  'LBL_ALLOW_OWNER' => '소유자',
  'LBL_ROLE' => '역활',
  'LBL_NAME' => '명칭',
  'LBL_DESCRIPTION' => '상세',
  'LIST_ROLES' => '역활리스트',
  'LBL_USERS_SUBPANEL_TITLE' => '유저',
  'LIST_ROLES_BY_USER' => '유저의 역활 리스트',
  'LBL_ROLES_SUBPANEL_TITLE' => '유저 역활',
  'LBL_SEARCH_FORM_TITLE' => '검색',
  'LBL_NO_ACCESS' => 'You do not have access to this area. Contact your site administrator to obtain access.',
  'LBL_REDIRECT_TO_HOME' => 'Redirect to Home in',
  'LBL_SECONDS' => 'seconds',
  'LBL_ADDING' => 'Adding for ',
);