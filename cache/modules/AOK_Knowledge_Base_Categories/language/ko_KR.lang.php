<?php
// created: 2015-12-29 21:02:17
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '입력일',
  'LBL_DATE_MODIFIED' => '편집일',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => '상세',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '작성자',
  'LBL_MODIFIED_USER' => '변경유저',
  'LBL_LIST_NAME' => '이름',
  'LBL_EDIT_BUTTON' => '편집',
  'LBL_REMOVE' => '삭제',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => '할당유저',
  'LBL_LIST_FORM_TITLE' => 'KB Categories List',
  'LBL_MODULE_NAME' => 'KB Categories',
  'LBL_MODULE_TITLE' => 'KB Categories',
  'LBL_HOMEPAGE_TITLE' => 'My KB Categories',
  'LNK_NEW_RECORD' => 'Create KB Categories',
  'LNK_LIST' => 'View KB Categories',
  'LNK_IMPORT_AOK_KB_CATEGORIES' => 'Import KB Categories',
  'LBL_SEARCH_FORM_TITLE' => 'Search KB Categories',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '영업활동',
  'LBL_AOK_KB_CATEGORIES_SUBPANEL_TITLE' => 'KB Categories',
  'LBL_NEW_FORM_TITLE' => 'New KB Categories',
  'LBL_AOK_KB_TITLE' => 'Knowledge Base',
);