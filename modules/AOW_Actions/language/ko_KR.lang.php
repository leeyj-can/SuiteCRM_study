<?php
/**
 * Advanced OpenWorkflow, Automating SugarCRM.
 * @package Advanced OpenWorkflow for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */


$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '입력일',
  'LBL_DATE_MODIFIED' => '편집일',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED_USER' => '작성자',
  'LBL_MODIFIED_USER' => '변경유저',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => '상세',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_MODULE_NAME' => 'WorkFlow Actions',
  'LBL_MODULE_TITLE' => 'WorkFlow Actions',
  'LBL_AOW_WORKFLOW_ID' => 'AOW_WorkFlow Id',
  'LBL_ACTION' => 'Action',
  'LBL_PARAMETERS' => 'Parameters',
  'LBL_SENDEMAIL' => 'Send Email',
  'LBL_CREATERECORD' => 'Create Record',
  'LBL_MODIFYRECORD' => 'Modify Record',
  'LBL_SELECT_ACTION' => 'Select Action',
  'LBL_CREATE_EMAIL_TEMPLATE' => '작성',
  'LBL_RECORD_TYPE' =>  'Record Type',
  'LBL_ADD_FIELD' => 'Add Field',
  'LBL_ADD_RELATIONSHIP' => 'Add Relationship',
  'LBL_EDIT_EMAIL_TEMPLATE' => '편집',
  'LBL_EMAIL' => 'E메일',
  'LBL_EMAIL_TEMPLATE' => 'Email Template',
  'LBL_SETAPPROVAL' => 'Set Approval',
  'LBL_RELATE_WORKFLOW' => 'Relate to WorkFlow Module',
  'LBL_INDIVIDUAL_EMAILS' => 'Send Individual Emails',
);
