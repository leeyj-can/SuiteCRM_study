<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_MODULE_NAME' => '타켓',
  'LBL_MODULE_ID'   => '타켓',
  'LBL_INVITEE' => '직속상관',
  'LBL_MODULE_TITLE' => 'Targets: Home',
  'LBL_SEARCH_FORM_TITLE' => 'Target Search',
  'LBL_LIST_FORM_TITLE' => '타켓리스트',
  'LBL_NEW_FORM_TITLE' => 'New Target',
  'LBL_PROSPECT' => 'Target:',
  'LBL_BUSINESSCARD' => '명함',
  'LBL_LIST_NAME' => '이름',
  'LBL_LIST_LAST_NAME' => '이름(명)',
  'LBL_LIST_PROSPECT_NAME' => 'Target Name',
  'LBL_LIST_TITLE' => '직위:',
  'LBL_LIST_EMAIL_ADDRESS' => 'E메일',
  'LBL_LIST_OTHER_EMAIL_ADDRESS' => '다른 E메일',
  'LBL_LIST_PHONE' => '전화:',
  'LBL_LIST_PROSPECT_ROLE' => '역활',
  'LBL_LIST_FIRST_NAME' => '이름(명):',
  'LBL_ASSIGNED_TO_NAME' => '할당유저',
  'LBL_ASSIGNED_TO_ID'=>'할당:',
//DON'T CONVERT THESE THEY ARE MAPPINGS
  'db_last_name' => 'LBL_LIST_LAST_NAME',
  'db_first_name' => 'LBL_LIST_FIRST_NAME',
  'db_title' => 'LBL_LIST_TITLE',
  'db_email1' => 'LBL_LIST_EMAIL_ADDRESS',
  'db_email2' => 'LBL_LIST_OTHER_EMAIL_ADDRESS',
//END DON'T CONVERT
  'LBL_CAMPAIGN_ID' => 'Campaign ID',
  'LBL_EXISTING_PROSPECT' => '기존 거래담당자 사용',
  'LBL_CREATED_PROSPECT' => '신규 거래담당자 작성',
  'LBL_EXISTING_ACCOUNT' => '기존 거래처 사용',
  'LBL_CREATED_ACCOUNT' => '신규거래처작성',
  'LBL_CREATED_CALL' => '신규 콜작성',
  'LBL_CREATED_MEETING' => '신규 미팅 작성',
  'LBL_ADDMORE_BUSINESSCARD' => '다른명함에서입력',
  'LBL_ADD_BUSINESSCARD' => '비지네스카드작성',
  'LBL_NAME' => '명칭:',
  'LBL_FULL_NAME' => '이름',
  'LBL_PROSPECT_NAME' => 'Target Name:',
  'LBL_PROSPECT_INFORMATION' => '리드 정보',
  'LBL_MORE_INFORMATION' => 'More Information',
  'LBL_FIRST_NAME' => '이름(성):',
  'LBL_OFFICE_PHONE' => '직장전화:',
  'LBL_ANY_PHONE' => '전화:',
  'LBL_PHONE' => '전화:',
  'LBL_LAST_NAME' => '이름(명):',
  'LBL_MOBILE_PHONE' => '휴대폰:',
  'LBL_HOME_PHONE' => '자택전화:',
  'LBL_OTHER_PHONE' => '연락 전화:',
  'LBL_FAX_PHONE' => '팩스:',
  'LBL_PRIMARY_ADDRESS_STREET' => '메인주소 주소:',
  'LBL_PRIMARY_ADDRESS_CITY' => '메인주소 시구군:',
  'LBL_PRIMARY_ADDRESS_COUNTRY' => '메인주소 국가:',
  'LBL_PRIMARY_ADDRESS_STATE' => '메인주소 읍면동:',
  'LBL_PRIMARY_ADDRESS_POSTALCODE' => '메인주소 우편번호:',
  'LBL_ALT_ADDRESS_STREET' => '부가주소 주소 1:',
  'LBL_ALT_ADDRESS_CITY' => '부가주소 시도구:',
  'LBL_ALT_ADDRESS_COUNTRY' => '부가주소 국가:',
  'LBL_ALT_ADDRESS_STATE' => '부가주소 동읍면:',
  'LBL_ALT_ADDRESS_POSTALCODE' => '부가주소  우편번호:',
  'LBL_TITLE' => '직위:',
  'LBL_DEPARTMENT' => '부문:',
  'LBL_BIRTHDATE' => '생년월일:',
  'LBL_EMAIL_ADDRESS' => 'E메일:',
  'LBL_OTHER_EMAIL_ADDRESS' => '연락E메일:',
  'LBL_ANY_EMAIL' => 'E메일:',
  'LBL_ASSISTANT' => '보조자:',
  'LBL_ASSISTANT_PHONE' => '보조자 전화:',
  'LBL_DO_NOT_CALL' => '전화불가:',
  'LBL_EMAIL_OPT_OUT' => '메일송신:',
  'LBL_PRIMARY_ADDRESS' => '메인주소:',
  'LBL_ALTERNATE_ADDRESS' => '연락주소:',
  'LBL_ANY_ADDRESS' => '연락주소:',
  'LBL_CITY' => '시구군:',
  'LBL_STATE' => '읍면동:',
  'LBL_POSTAL_CODE' => '우편번호:',
  'LBL_COUNTRY' => '국가:',
  'LBL_DESCRIPTION_INFORMATION' => '상세정보',
  'LBL_ADDRESS_INFORMATION' => '주소 정보',
  'LBL_DESCRIPTION' => '상세:',
  'LBL_PROSPECT_ROLE' => '역활:',
  'LBL_OPP_NAME' => '안건명:',
  'LBL_IMPORT_VCARD' => 'vCard에서작성',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new contact by importing a vCard from your file system.',
  'LBL_DUPLICATE' => 'Possible Duplicate Targets',
  'MSG_SHOW_DUPLICATES' => 'The target record you are about to create might be a duplicate of a target record that already exists. Target records containing similar names and/or email addresses are listed below.<br>Click Create Target to continue creating this new target, or select an existing target listed below.',
  'MSG_DUPLICATE' => 'The target record you are about to create might be a duplicate of a target record that already exists. Target records containing similar names and/or email addresses are listed below.<br>Click Save to continue creating this new target, or click Cancel to return to the module without creating the target.',
  'LNK_IMPORT_VCARD' => 'Create From vCard',
  'LNK_NEW_ACCOUNT' => '거래처 작성',
  'LNK_NEW_OPPORTUNITY' => '안건 작성',
  'LNK_NEW_CASE' => '사례작성',
  'LNK_NEW_NOTE' => '노트작성',
  'LNK_NEW_CALL' => '콜스케쥴작성',
  'LNK_NEW_EMAIL' => '메일작성',
  'LNK_NEW_MEETING' => '미팅스케쥴작성',
  'LNK_NEW_TASK' => '타스크작성',
  'LNK_NEW_APPOINTMENT' => '약속작성',
  'LNK_IMPORT_PROSPECTS' => 'Import Targets',
  'NTC_DELETE_CONFIRMATION' => 'Are you sure you want to delete this record?',
  'NTC_REMOVE_CONFIRMATION' => 'Are you sure you want to remove this contact from this case?',
  'NTC_REMOVE_DIRECT_REPORT_CONFIRMATION' => 'Are you sure you want to remove this record as a direct report?',
  'ERR_DELETE_RECORD' => 'A record number must be specified to delete the contact.',
  'NTC_COPY_PRIMARY_ADDRESS' => 'Copy primary address to alternate address',
  'NTC_COPY_ALTERNATE_ADDRESS' => 'Copy alternate address to primary address',
  'LBL_SALUTATION' => '경칭',
  'LBL_SAVE_PROSPECT' => 'Save Target',
  'LBL_CREATED_OPPORTUNITY' =>'신규 안건 작성',
  'NTC_OPPORTUNITY_REQUIRES_ACCOUNT' => 'Creating an opportunity requires an account.\n Please either create a new one or select an existing one.',
  'LNK_SELECT_ACCOUNT' => "거래처 선택",
  'LNK_NEW_PROSPECT' => '타켓작성',
  'LNK_PROSPECT_LIST' => '타켓리스트',
  'LNK_NEW_CAMPAIGN' => 'Create Campaign',
  'LNK_CAMPAIGN_LIST' => '캠페인',
  'LNK_NEW_PROSPECT_LIST' => '타켓리스트작성',
  'LNK_PROSPECT_LIST_LIST' => '타켓리스트',
  'LNK_IMPORT_PROSPECT' => 'Import Targets',
  'LBL_SELECT_CHECKED_BUTTON_LABEL' => 'Select Checked Targets',
  'LBL_SELECT_CHECKED_BUTTON_TITLE' => 'Select Checked Targets',
  'LBL_INVALID_EMAIL'=>'무효E메일:',
  'LBL_DEFAULT_SUBPANEL_TITLE'=>'타켓',
  'LBL_PROSPECT_LIST' => 'Prospect List',
  'LBL_CONVERT_BUTTON_KEY' => 'V',
  'LBL_CONVERT_BUTTON_TITLE' => 'Convert Target',
  'LBL_CONVERT_BUTTON_LABEL' => 'Convert Target',
  'LBL_CONVERTPROSPECT'=>'Convert Target',
  'LNK_NEW_CONTACT'=>'거래고객작성',
  'LBL_CREATED_CONTACT'=>"신규 거래담당자 작성",
  'LBL_BACKTO_PROSPECTS'=>'Back to Targets',
  'LBL_CAMPAIGNS'=>'캠페인',
  'LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'=>'Campaign Log',
  'LBL_TRACKER_KEY'=>'Tracker Key',
  'LBL_LEAD_ID'=>'Lead Id',
  'LBL_CONVERTED_LEAD'=>'Converted Lead',
  'LBL_ACCOUNT_NAME'=>'거래처명',
  'LBL_EDIT_ACCOUNT_NAME'=>'거래처명:',
  'LBL_CREATED_USER' => 'Created User',
  'LBL_MODIFIED_USER' => 'Modified User',
  'LBL_CAMPAIGNS_SUBPANEL_TITLE' => '캠페인',
  'LBL_HISTORY_SUBPANEL_TITLE'=>'이력',
  //For export labels
  'LBL_PHONE_HOME' => 'Phone Home',
  'LBL_PHONE_MOBILE' => '모바일:',
  'LBL_PHONE_WORK' => 'Phone Work',
  'LBL_PHONE_OTHER' => 'Phone Other',
  'LBL_PHONE_FAX' => '팩스:',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
  'LBL_EXPORT_ASSIGNED_USER_ID' => 'Assigned User ID',
  'LBL_EXPORT_MODIFIED_USER_ID' => 'Modified By ID',
  'LBL_EXPORT_CREATED_BY' => 'Created By ID',
  'LBL_EXPORT_EMAIL2'=>'Other Email Address',
);
?>
