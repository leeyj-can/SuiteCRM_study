<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description: Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
	'LBL_ADD_ANOTHER_FILE'		=> 'Add Another File',
	'LBL_ADD_DOCUMENT'			=> 'Add a Document',
	'LBL_ADD_FILE'				=> 'Add a file',
	'LBL_ATTACHMENTS'			=> '첨부파일',
	'LBL_BODY'					=> 'Body:',
	'LBL_CLOSE'					=> '종료:',
	'LBL_COLON'					=> ':',
	'LBL_DESCRIPTION'			=> '상세:',
	'LBL_EDIT_ALT_TEXT'			=> 'Edit Plain Text',
	'LBL_EMAIL_ATTACHMENT'		=> 'Email Attachment',
	'LBL_HIDE_ALT_TEXT'			=> 'Hide Plain Text',
	'LBL_HTML_BODY'				=> 'HTML Body',
	'LBL_INSERT_VARIABLE'		=> 'Insert Variable:',
	'LBL_INSERT_URL_REF'		=> 'Insert URL Reference',
	'LBL_INSERT_TRACKER_URL'	=> 'Insert Tracker URL:',
	'LBL_INSERT'				=> 'Insert',
	'LBL_LIST_DATE_MODIFIED'	=> '최종변경일',
	'LBL_LIST_DESCRIPTION'		=> '상세',
	'LBL_LIST_FORM_TITLE'		=> 'Email Templates List',
	'LBL_LIST_NAME'				=> '이름',
	'LBL_MODULE_NAME'			=> 'E메일템프렛',
	'LBL_MODULE_TITLE'			=> 'Email Templates: Home',
	'LBL_NAME'					=> '명칭:',
	'LBL_NEW_FORM_TITLE'		=> 'E메일 템플렛 작성',
	'LBL_PUBLISH'				=> 'Publish:',
	'LBL_RELATED_TO'			=> '관련처:',
	'LBL_SEARCH_FORM_TITLE'		=> 'Email Templates Search',
	'LBL_SHOW_ALT_TEXT'			=> 'Show Plain Text',
	'LBL_SUBJECT'				=> '제목:',
    'LBL_SUGAR_DOCUMENT'        => '자료 리스트',
	'LBL_TEAMS'					=> 'Teams:',
	'LBL_TEAMS_LINK'			=> '부서',
	'LBL_TEXT_BODY'				=> 'Text Body',
	'LBL_USERS'					=> '유저:',

	'LNK_ALL_EMAIL_LIST'		=> '전메일',
	'LNK_ARCHIVED_EMAIL_LIST'	=> 'Archived Emails',
	'LNK_CHECK_EMAIL'			=> '체크메일',
	'LNK_DRAFTS_EMAIL_LIST'		=> 'All Drafts',
	'LNK_EMAIL_TEMPLATE_LIST'	=> 'E메일 템플렛',
	'LNK_IMPORT_NOTES'			=> '노트읽어들이기',
	'LNK_NEW_ARCHIVE_EMAIL'		=> 'Create Archived Email',
	'LNK_NEW_EMAIL_TEMPLATE'	=> 'E메일 템플렛 작성',
	'LNK_NEW_EMAIL'				=> '메일작성',
	'LNK_NEW_SEND_EMAIL'		=> '이메일작성',
	'LNK_SENT_EMAIL_LIST'		=> '송신한메일',
	'LNK_VIEW_CALENDAR'			=> '오늘',
	// for Inbox
	'LBL_NEW'					=> '신규',
	'LNK_CHECK_MY_INBOX'		=> '메일체크',
	'LNK_GROUP_INBOX'			=> '그룹수신함',
	'LNK_MY_ARCHIVED_LIST'		=> 'My Archives',
	'LNK_MY_DRAFTS'				=> 'My Drafts',
	'LNK_MY_INBOX'				=> 'My Email',
	'LBL_LIST_BASE_MODULE'		=> 'Base Module:',
    'LBL_TEXT_ONLY'             => 'Text Only',
    'LBL_SEND_AS_TEXT'          => 'Send Text Only',
    'LBL_ACCOUNT'               => '거래처',
    'LBL_BASE_MODULE'=>'Base Module',
    'LBL_FROM_NAME'=>'From Name',
    'LBL_PLAIN_TEXT'=>'Plain Text',
    'LBL_CREATED_BY'=>'Created By',
    'LBL_FROM_ADDRESS'=>'From Address',
    'LBL_PUBLISHED'=>'Published',
    'LBL_ACTIVITIES_REPORTS' 	=> 'Activities Report',
    'LNK_VIEW_MY_INBOX' => 'View My Email',
	'LBL_ASSIGNED_TO_ID' => '할당',
	'LBL_EDIT_LAYOUT' => 'Edit Layout' /*for 508 compliance fix*/,
	'LBL_SELECT' => '선택[Alt+T]' /*for 508 compliance fix*/,
	'LBL_ID_FF_CLEAR' => '지우기[Alt+C]' /*for 508 compliance fix*/,
    'LBL_TYPE' => '종류',
);
?>
