<?php
/**
 *
 * @package Advanced OpenPortal
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author Salesagility Ltd <support@salesagility.com>
 */

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => '할당유저',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '입력일',
  'LBL_DATE_MODIFIED' => '편집일',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => '상세',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '작성자',
  'LBL_MODIFIED_USER' => '변경유저',
  'LBL_LIST_NAME' => '이름',
  'LBL_EDIT_BUTTON' => '편집',
  'LBL_REMOVE' => '삭제',
  'LBL_LIST_FORM_TITLE' => 'Case Updates List',
  'LBL_MODULE_NAME' => 'Case Updates',
  'LBL_MODULE_TITLE' => 'Case Updates',
  'LBL_HOMEPAGE_TITLE' => 'My Case Updates',
  'LNK_NEW_RECORD' => 'Create Case Updates',
  'LNK_LIST' => 'View Case Updates',
  'LNK_IMPORT_AOP_AOP_CASE_UPDATES' => 'Import Case Updates',
  'LBL_SEARCH_FORM_TITLE' => 'Search Case Updates',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '영업활동',
  'LBL_AOP_AOP_CASE_UPDATES_SUBPANEL_TITLE' => 'Case Updates',
  'LBL_NEW_FORM_TITLE' => 'New Case Updates',
  'LNK_IMPORT_AOP_CASE_UPDATES' => 'Import Case Updates',
  'LBL_AOP_CASE_UPDATES_SUBPANEL_TITLE' => 'Case Updates',
  'LBL_CASE_NAME' => '사례',
  'LBL_CONTACT_NAME' => '거래담당자',
  'LBL_INTERNAL' => 'Internal Update',    
  'LBL_AOP_CASE_ATTACHMENTS' => 'Attachments: ', 
);