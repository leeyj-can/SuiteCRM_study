<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/




$mod_strings = array (
	'LBL_MODULE_NAME' => '프로젝트',
	'LBL_MODULE_TITLE' => '프로젝트: 홈',
	'LBL_SEARCH_FORM_TITLE' => '프로젝트 검색',
    'LBL_LIST_FORM_TITLE' => '프로젝트 리스트',
    'LBL_HISTORY_TITLE' => '이력',

	'LBL_ID' => 'Id:',
	'LBL_DATE_ENTERED' => '입력일:',
	'LBL_DATE_MODIFIED' => '편집일:',
	'LBL_ASSIGNED_USER_ID' => '할당:',
    'LBL_ASSIGNED_USER_NAME' => '할당유저:',
	'LBL_MODIFIED_USER_ID' => '편집유저Id:',
	'LBL_CREATED_BY' => '등록자:',
	'LBL_TEAM_ID' => '부서:',
	'LBL_NAME' => '명칭:',
    'LBL_PDF_PROJECT_NAME' => 'Project Name:',
	'LBL_DESCRIPTION' => '상세:',
	'LBL_DELETED' => '삭제:',
    'LBL_DATE' => '날짜',
	'LBL_DATE_START' => '시작일:',
	'LBL_DATE_END' => 'End Date:',
	'LBL_PRIORITY' => '우선순위:',
    'LBL_STATUS' => '상태:',
    'LBL_MY_PROJECTS' => 'My Projects',
    'LBL_MY_PROJECT_TASKS' => 'My Project Tasks',
    
	'LBL_TOTAL_ESTIMATED_EFFORT' => '견적시간계 (시간):',
	'LBL_TOTAL_ACTUAL_EFFORT' => '실질시간계 (시간):',

	'LBL_LIST_NAME' => '명칭',
    'LBL_LIST_DAYS' => 'days',
	'LBL_LIST_ASSIGNED_USER_ID' => '할당',
	'LBL_LIST_TOTAL_ESTIMATED_EFFORT' => '견적시간계 (시간)',
	'LBL_LIST_TOTAL_ACTUAL_EFFORT' => '실질시간계 (시간)',
    'LBL_LIST_UPCOMING_TASKS' => 'Upcoming Tasks (1 Week)',
    'LBL_LIST_OVERDUE_TASKS' => 'Overdue Tasks',
    'LBL_LIST_OPEN_CASES' => 'Open Cases',
    'LBL_LIST_END_DATE' => '종료일',
    'LBL_LIST_TEAM_ID' => 'Team',
    

	'LBL_PROJECT_SUBPANEL_TITLE' => '프로젝트',
	'LBL_PROJECT_TASK_SUBPANEL_TITLE' => '프로젝트 타스크',
	'LBL_CONTACT_SUBPANEL_TITLE' => '거래담당자',
	'LBL_ACCOUNT_SUBPANEL_TITLE' => '거래처',
	'LBL_OPPORTUNITY_SUBPANEL_TITLE' => '안건',
	'LBL_QUOTE_SUBPANEL_TITLE' => '견적',

    // quick create label
    'LBL_NEW_FORM_TITLE' => '프로젝트 작성',

	'CONTACT_REMOVE_PROJECT_CONFIRM' => 'Are you sure you want to remove this contact from this project?',

	'LNK_NEW_PROJECT'	=> '프로젝트 작성',
	'LNK_PROJECT_LIST'	=> '프로젝트 리스트',
	'LNK_NEW_PROJECT_TASK'	=> '프로젝트 타스크작성',
	'LNK_PROJECT_TASK_LIST'	=> '프로젝트 타스크',
	
	'LBL_DEFAULT_SUBPANEL_TITLE' => '프로젝트',
	'LBL_ACTIVITIES_TITLE'=>'영업활동',
    'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'영업활동',
	'LBL_HISTORY_SUBPANEL_TITLE'=>'이력',
	'LBL_QUICK_NEW_PROJECT'	=> '프로젝트 작성',
	
	'LBL_PROJECT_TASKS_SUBPANEL_TITLE' => '프로젝트 타스크',
	'LBL_CONTACTS_SUBPANEL_TITLE' => '거래담당자',
	'LBL_ACCOUNTS_SUBPANEL_TITLE' => '거래처',
	'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => '안건',
    'LBL_CASES_SUBPANEL_TITLE' => '사례리스트',
    'LBL_BUGS_SUBPANEL_TITLE' => '결함리스트',
    'LBL_PRODUCTS_SUBPANEL_TITLE' => '제품',
    

    'LBL_TASK_ID' => 'ID',
    'LBL_TASK_NAME' => 'Task Name',
    'LBL_DURATION' => '시간',
    'LBL_ACTUAL_DURATION' => 'Actual Duration',
    'LBL_START' => '최초',
    'LBL_FINISH' => 'Finish',
    'LBL_PREDECESSORS' => 'Predecessors',
    'LBL_PERCENT_COMPLETE' => '진척률(%)',
    'LBL_MORE'  => 'More...',

    'LBL_PERCENT_BUSY' => '% Busy',
    'LBL_TASK_ID_WIDGET' => 'ID',
    'LBL_TASK_NAME_WIDGET' => 'description',
    'LBL_DURATION_WIDGET' => 'duration',
    'LBL_START_WIDGET' => 'date_start',
    'LBL_FINISH_WIDGET' => 'date_finish',
    'LBL_PREDECESSORS_WIDGET' => 'predecessors_',
    'LBL_PERCENT_COMPLETE_WIDGET' => 'percent_complete',
    'LBL_EDIT_PROJECT_TASKS_TITLE'=> 'Edit Project Tasks',
    
    'LBL_OPPORTUNITIES' => '안건',
	'LBL_LAST_WEEK' => '이전',
	'LBL_NEXT_WEEK' => '다음',
	'LBL_PROJECTRESOURCES_SUBPANEL_TITLE' => 'Project Resources',
	'LBL_PROJECTTASK_SUBPANEL_TITLE' => '프로젝트타스크',
	'LBL_HOLIDAYS_SUBPANEL_TITLE' => 'Holidays',
	'LBL_PROJECT_INFORMATION' => 'Project Overview',
	'LBL_EDITLAYOUT' => 'Edit Layout' /*for 508 compliance fix*/,
	'LBL_INSERTROWS' => 'Insert Rows' /*for 508 compliance fix*/,
);
?>
