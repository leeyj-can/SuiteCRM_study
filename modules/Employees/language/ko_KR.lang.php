<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_MODULE_NAME' => '종업원',
  'LBL_MODULE_TITLE' => '종업원: 홈',
  'LBL_SEARCH_FORM_TITLE' => '종업원 검색',
  'LBL_LIST_FORM_TITLE' => '종업원',
  'LBL_NEW_FORM_TITLE' => '종업원 작성',
  'LBL_EMPLOYEE' => '종업원:',
  'LBL_LOGIN' => '로그인',
  'LBL_RESET_PREFERENCES' => 'Reset To Default Preferences',
  'LBL_TIME_FORMAT' => '시간표시형식:',
  'LBL_DATE_FORMAT' => '일자표시형식:',
  'LBL_TIMEZONE' => '현재시간:',
  'LBL_CURRENCY' => '통화:',
  'LBL_LIST_NAME' => '이름',
  'LBL_LIST_LAST_NAME' => '이름(성)',
  'LBL_LIST_EMPLOYEE_NAME' => 'Employee Name',
  'LBL_LIST_DEPARTMENT' => '부서',
  'LBL_LIST_REPORTS_TO_NAME' => '직속상사',
  'LBL_LIST_EMAIL' => '메일',
  'LBL_LIST_PRIMARY_PHONE' => '전화',
  'LBL_LIST_USER_NAME' => '유저명',
  'LBL_LIST_ADMIN' => '관리',
  'LBL_NEW_EMPLOYEE_BUTTON_TITLE' => '종업원작성[Alt+N]',
  'LBL_NEW_EMPLOYEE_BUTTON_LABEL' => '종업원작성',
  'LBL_NEW_EMPLOYEE_BUTTON_KEY' => 'N',
  'LBL_ERROR' => 'Error:',
  'LBL_PASSWORD' => '패스워드:',
  'LBL_EMPLOYEE_NAME' => '종업원 이름:',
  'LBL_USER_NAME' => '유저명:',
  'LBL_USER_TYPE' => 'User Type',
  'LBL_FIRST_NAME' => '아름(명):',
  'LBL_LAST_NAME' => '이름(성):',
  'LBL_EMPLOYEE_SETTINGS' => '종업원 설정',
  'LBL_THEME' => '썸:',
  'LBL_LANGUAGE' => '언어:',
  'LBL_ADMIN' => '관리자:',
  'LBL_EMPLOYEE_INFORMATION' => '종업원 정보',
  'LBL_OFFICE_PHONE' => '사무실전화:',
  'LBL_REPORTS_TO' => '직속상사:',
  'LBL_REPORTS_TO_NAME' => '직속상사:',
  'LBL_OTHER_PHONE' => '다른전화:',
  'LBL_OTHER_EMAIL' => '다른메일:',
  'LBL_NOTES' => '노트:',
  'LBL_DEPARTMENT' => '부서:',
  'LBL_TITLE' => '작위:',
  'LBL_ANY_ADDRESS' => '연락주소:',
  'LBL_ANY_PHONE' => '전화:',
  'LBL_ANY_EMAIL' => 'E메일:',
  'LBL_ADDRESS' => '주소:',
  'LBL_CITY' => '시구군:',
  'LBL_STATE' => '읍면동:',
  'LBL_POSTAL_CODE' => '우편번호:',
  'LBL_COUNTRY' => '국가:',
  'LBL_NAME' => '이름:',
  'LBL_MOBILE_PHONE' => '모바일:',
  'LBL_OTHER' => '기타:',
  'LBL_FAX' => '팩스:',
  'LBL_EMAIL' => 'E메일:',
  'LBL_EMAIL_LINK_TYPE'				=> 'E메일 클라이언트',
  'LBL_EMAIL_LINK_TYPE_HELP'			=> '<b>SuiteCRM Mail Client:</b> Send emails using the email client in the SuiteCRM application.<br><b>External Mail Client:</b> Send email using an email client outside of the SuiteCRM application, such as Microsoft Outlook.',
  'LBL_HOME_PHONE' => '자택전화:',
  'LBL_WORK_PHONE' => 'Work Phone:',
  'LBL_EMPLOYEE_STATUS' => '상태:',
  'LBL_PRIMARY_ADDRESS' => '주소:',
  'LBL_SAVED_SEARCH' => '레이아웃옵션',
  'LBL_CREATE_USER_BUTTON_TITLE' => '유저 작성[Alt+N]',
  'LBL_CREATE_USER_BUTTON_LABEL' => '유저 작성',
  'LBL_CREATE_USER_BUTTON_KEY' => 'N',
  'LBL_FAVORITE_COLOR' => '좋아하는색깔:',
  'LBL_MESSENGER_ID' => 'IM 명:',
  'LBL_MESSENGER_TYPE' => 'IM 종류:',
  'ERR_EMPLOYEE_NAME_EXISTS_1' => '종업원 이름',
  'ERR_EMPLOYEE_NAME_EXISTS_2' => ' already exists.  Duplicate employee names are not allowed.  Change the employee name to be unique.',
  'ERR_LAST_ADMIN_1' => '종업원 이름',
  'ERR_LAST_ADMIN_2' => '" is the last employee with administrator access.  At least one employee must be an administrator.',
  'LNK_NEW_EMPLOYEE' => '종업원 작성',
  'LNK_EMPLOYEE_LIST' => '종업원리스트',
  'ERR_DELETE_RECORD' => 'You must specify a record number to delete the account.',
  'LBL_LIST_EMPLOYEE_STATUS' => '상태',

  'LBL_SUGAR_LOGIN' => 'Is User',
  'LBL_RECEIVE_NOTIFICATIONS' => '할당통보:',  
  'LBL_IS_ADMIN' => '관리자',  
  'LBL_GROUP' => '그룹유저',
  'LBL_PORTAL_ONLY'	=> 'Portal Only User',
  'LBL_PHOTO'	=> '사진',
  'LBL_DELETE_USER_CONFIRM'           => 'This Employee is also a User. Deleting the Employee record will also delete the User record, and the User will no longer be able to access the application. Do you want to proceed with deleting this record?',
  'LBL_DELETE_EMPLOYEE_CONFIRM'       => 'Are you sure you want to delete this employee?',
  'LBL_ONLY_ACTIVE' => 'Active Employees',
	'LBL_SELECT' => '선택[Alt+T]' /*for 508 compliance fix*/,
	'LBL_FF_CLEAR' => '지우기[Alt+C]' /*for 508 compliance fix*/,
  'LBL_AUTHENTICATE_ID' => 'Authentication Id',
  'LBL_EXT_AUTHENTICATE' => 'External Authentication',
  'LBL_GROUP_USER' => '그룹유저',
  'LBL_LIST_ACCEPT_STATUS' => '허가상태',
  'LBL_MODIFIED_BY' =>'Modified By',
  'LBL_MODIFIED_BY_ID' =>'Modified By Id',
  'LBL_CREATED_BY_NAME' => 'Created By', //bug48978
  'LBL_PORTAL_ONLY_USER' => 'Portal API User',
  'LBL_PSW_MODIFIED' => 'Password Last Changed',
  'LBL_SHOW_ON_EMPLOYEES' => 'Display Employee Record',
  'LBL_USER_HASH' => '패스워드:',
  'LBL_SYSTEM_GENERATED_PASSWORD' =>'System Generated Password',
  'LBL_DESCRIPTION'	=> '상세',
  'LBL_FAX_PHONE'						=> '팩스:',
  'LBL_STATUS'						=> '상태',
  'LBL_ADDRESS_CITY'					=> '시구군:',
  'LBL_ADDRESS_COUNTRY'				=> '국가:',
  'LBL_ADDRESS_INFORMATION'			=> '주소 정보',
  'LBL_ADDRESS_POSTALCODE'			=> '우편번호:',
  'LBL_ADDRESS_STATE'					=> '읍면동:',
  'LBL_ADDRESS_STREET'				=> '나머지주소:',
  
  'LBL_DATE_MODIFIED' => '편집일',
  'LBL_DATE_ENTERED' => 'Date Entered',
  'LBL_DELETED' => '삭제',
);


?>
