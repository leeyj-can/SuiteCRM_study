<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
	'ERR_DELETE_RECORD' => 'A record number must be specified to delete the meeting.',

	'LBL_ACCEPT_THIS'=>'허가?',
	'LBL_ADD_BUTTON'=> '추가',
	'LBL_ADD_INVITEE' => '참가자추가',
	'LBL_COLON' => ':',
	'LBL_CONTACT_NAME' => '거래담당자:',
	'LBL_CONTACTS_SUBPANEL_TITLE' => '거래담당자',
	'LBL_CREATED_BY'=>'등록자',
	'LBL_DATE_END'=>'종료일',
	'LBL_DATE_TIME' => '시작일시:',
	'LBL_DATE' => '시작일:',
	'LBL_DEFAULT_SUBPANEL_TITLE' => '미팅',
	'LBL_DEL'=> '삭제',
	'LBL_DESCRIPTION_INFORMATION' => '상세정보',
	'LBL_DESCRIPTION' => '상세:',
    'LBL_DIRECTION' => '방향:',
	'LBL_DURATION_HOURS' => '시간:',
	'LBL_DURATION_MINUTES' => '분:',
	'LBL_DURATION' => '시간:',
	'LBL_EMAIL' => 'E메일',
	'LBL_FIRST_NAME' => '이름(성)',
	'LBL_HISTORY_SUBPANEL_TITLE' => '노트',
	'LBL_HOURS_ABBREV' => 'h',
	'LBL_HOURS_MINS' => '(시간/분)',
	'LBL_INVITEE' => '참가자',
	'LBL_LAST_NAME' => '이름(명)',
	'LBL_ASSIGNED_TO_NAME'=>'할당유저:',
	'LBL_LIST_ASSIGNED_TO_NAME' => '할당유저',
	'LBL_LIST_CLOSE' => '종료',
	'LBL_LIST_CONTACT' => '거래 담당자',
	'LBL_LIST_DATE_MODIFIED'=>'편집일',
	'LBL_LIST_DATE' => '시작일',
	'LBL_LIST_DIRECTION' => '방향',
	'LBL_LIST_DUE_DATE'=>'만료일',
	'LBL_LIST_FORM_TITLE' => '미팅 리스트',
	'LBL_LIST_MY_MEETINGS' => 'My 미팅 리스트',
	'LBL_LIST_RELATED_TO' => '관련',
	'LBL_LIST_STATUS'=>'상태',
	'LBL_LIST_SUBJECT' => '제목',
	'LBL_LIST_TIME' => '시작시간',
	'LBL_LEADS_SUBPANEL_TITLE' => '리드',
	'LBL_LOCATION' => '장소:',
	'LBL_MEETING' => '미팅:',
	'LBL_MINSS_ABBREV' => 'm',
	'LBL_MODIFIED_BY'=>'편집자',
	'LBL_MODULE_NAME' => '미팅',
	'LBL_MODULE_TITLE' => '미팅: Home',
	'LBL_NAME' => '명칭',
	'LBL_NEW_FORM_TITLE' => '미팅 스케쥴 작성',
	'LBL_OUTLOOK_ID' => '아웃룩ID',
	'LBL_SEQUENCE' => 'Meeting update sequence',
	'LBL_PHONE' => '직장전화:',
	'LBL_REMINDER_TIME'=>'통보시간',
    'LBL_EMAIL_REMINDER_SENT' => 'Email reminder sent',
	'LBL_REMINDER' => '통보:',
	'LBL_REMINDER_POPUP' => 'Popup',
	'LBL_REMINDER_EMAIL' => 'E메일',
    'LBL_REMINDER_EMAIL_ALL_INVITEES' => 'Email all invitees',
    'LBL_EMAIL_REMINDER' => 'Email Reminder',
    'LBL_EMAIL_REMINDER_TIME' => 'Email Reminder Time',
    'LBL_REMOVE' => 'rem',
	'LBL_SCHEDULING_FORM_TITLE' => '스케쥴',
	'LBL_SEARCH_BUTTON'=> '검색',
	'LBL_SEARCH_FORM_TITLE' => '미팅 검색',
	'LBL_SEND_BUTTON_KEY'=>'I',
	'LBL_SEND_BUTTON_LABEL'=>'초대메일송신',
	'LBL_SEND_BUTTON_TITLE'=>'초대메일송신[Alt+I]',
	'LBL_STATUS' => '상태:',
    'LBL_TYPE' => 'Meeting Type',
    'LBL_PASSWORD' => 'Meeting Password',
    'LBL_URL' => 'Start/Join Meeting',
    'LBL_HOST_URL' => 'Host URL',
    'LBL_DISPLAYED_URL' => 'Display URL',
    'LBL_CREATOR' => 'Meeting Creator',
    'LBL_EXTERNALID' => 'External App ID',
	'LBL_SUBJECT' => '제목:',
	'LBL_TIME' => '시간시간:',
	'LBL_USERS_SUBPANEL_TITLE' => '유저',
	'LBL_ACTIVITIES_REPORTS' => 'Activities Report',
    'LBL_PARENT_TYPE' => '패런트종류',
    'LBL_PARENT_ID' => 'Parent ID',
	'LNK_MEETING_LIST'=>'미팅리스트',
	'LNK_NEW_APPOINTMENT' => '약속작성',
	'LNK_NEW_MEETING'=>'미팅스케쥴작성',
	'LNK_IMPORT_MEETINGS' => 'Import Meetings',

	'NTC_REMOVE_INVITEE' => 'Are you sure you want to remove this invitee from the meeting?',
    'LBL_CREATED_USER' => 'Created User',
    'LBL_MODIFIED_USER' => 'Modified User',
    'NOTICE_DURATION_TIME' => 'Duration time must be greater than 0',
    'LBL_MEETING_INFORMATION' => '리드 정보',
	'LBL_LIST_JOIN_MEETING' => 'Join Meeting',
	'LBL_JOIN_EXT_MEETING' => 'Join Meeting',
	'LBL_HOST_EXT_MEETING' => 'Start Meeting',
    'LBL_ACCEPT_STATUS' => '허가상태',
    'LBL_ACCEPT_LINK' => 'Accept Link',
    // You are not invited to the meeting messages
    'LBL_EXTNOT_HEADER' => 'Error: Not Invited',
    'LBL_EXTNOT_MAIN' => 'You are not able to join this meeting because you are not an Invitee.',
    'LBL_EXTNOT_RECORD_LINK' => 'View Meeting',
    'LBL_EXTNOT_GO_BACK' => 'Go back to the previous record',

    //cannot start messages
    'LBL_EXTNOSTART_HEADER' => 'Error: Cannot Start Meeting',
    'LBL_EXTNOSTART_MAIN' => 'You cannot start this meeting because you are not an Administrator or the owner of the meeting.',

  //For export labels
    'LBL_EXPORT_JOIN_URL' => 'Join Url',
    'LBL_EXPORT_HOST_URL' => 'Host Url',
    'LBL_EXPORT_DISPLAYED_URL' => 'Displayed Url',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Assigned User ID',
    'LBL_EXPORT_EXTERNAL_ID' => 'External ID',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Modified By ID',
    'LBL_EXPORT_CREATED_BY' => 'Created By ID',

    'LBL_EXPORT_DATE_START' => 'Start Date and Time',
    'LBL_EXPORT_DATE_END' => 'End Date and Time',
    'LBL_EXPORT_PARENT_TYPE' => 'Related Type',
    'LBL_EXPORT_PARENT_ID' => 'Parent ID',
    'LBL_EXPORT_REMINDER_TIME' =>'Reminder Time (in minutes)',

    // create invitee functionallity
    'LBL_CREATE_INVITEE' => 'Create an invitee',
    'LBL_CREATE_CONTACT' => 'As Contact',
    'LBL_CREATE_LEAD' => 'As Lead',
    'LBL_CREATE_AND_ADD' => 'Create & Add',
    'LBL_CANCEL_CREATE_INVITEE' => '취소 [Alt+X]',
    'LBL_EMPTY_SEARCH_RESULT' => 'Sorry, no results were found. Please create an invitee below.',
    'LBL_NO_ACCESS' => 'You have no access to create $module',
    
    'LBL_REPEAT_TYPE' => 'Repeat Type',
    'LBL_REPEAT_INTERVAL' => 'Repeat Interval',
    'LBL_REPEAT_DOW' => 'Repeat Dow',
    'LBL_REPEAT_UNTIL' => 'Repeat Until',
    'LBL_REPEAT_COUNT' => 'Repeat Count',
    'LBL_REPEAT_PARENT_ID' => 'Repeat Parent ID',
    'LBL_RECURRING_SOURCE' => 'Recurring Source',
    
    'LBL_SYNCED_RECURRING_MSG' => 'This meeting originated in another system and was synced to SuiteCRM. To make changes, go to the original meeting within the other system. Changes made in the other system can be synced to this record.',
    'LBL_RELATED_TO' => 'Related to:',

	// for reminders
	'LBL_REMINDERS' => '통보?',
	'LBL_REMINDERS_ACTIONS' => 'Actions:',
	'LBL_REMINDERS_POPUP' => 'Popup',
	'LBL_REMINDERS_EMAIL' => 'Email invitees',
	'LBL_REMINDERS_WHEN' => 'When:',
	'LBL_REMINDERS_REMOVE_REMINDER' => 'Remove reminder',
	'LBL_REMINDERS_ADD_ALL_INVITEES' => 'Add All Invitees',
	'LBL_REMINDERS_ADD_REMINDER' => 'Add reminder',
);
?>
