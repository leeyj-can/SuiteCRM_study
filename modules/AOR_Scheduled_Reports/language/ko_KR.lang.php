<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => '할당유저',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '입력일',
  'LBL_DATE_MODIFIED' => '편집일',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => '상세',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '작성자',
  'LBL_MODIFIED_USER' => '변경유저',
  'LBL_LIST_NAME' => '이름',
  'LBL_EDIT_BUTTON' => '편집',
  'LBL_REMOVE' => '삭제',
  'LBL_ANY_EMAIL' => 'E메일:',
  'LBL_EMAIL_NON_PRIMARY' => '비주요메일',
  'LBL_ASSIGNED_TO' => '할당유저:',
  'LBL_ASSIGNED_USER' => '할당유저:',
  'LBL_EMAIL_RECIPIENTS' => 'Email Recipients:',
  'LBL_USERS_ASSIGNED_LINK' => '할당유저',
  'LBL_USERS_CREATED_LINK' => '작성유저',
  'LBL_USERS_MODIFIED_LINK' => '변경유저',
  'NTC_DELETE_CONFIRMATION' => 'Are you sure you want to delete this record?',
  'LBL_LIST_FORM_TITLE' => 'Scheduled Reports List',
  'LBL_MODULE_NAME' => 'Scheduled Reports',
  'LBL_MODULE_TITLE' => 'Scheduled Reports',
  'LBL_HOMEPAGE_TITLE' => 'My Scheduled Reports',
  'LNK_NEW_RECORD' => 'Create Scheduled Reports',
  'LNK_LIST' => 'View Scheduled Reports',
  'LNK_IMPORT_AOR_SCHEDULED_REPORTS' => 'Import Scheduled Reports',
  'LBL_SEARCH_FORM_TITLE' => 'Search Scheduled Reports',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_AOR_SCHEDULED_REPORTS_SUBPANEL_TITLE' => 'Scheduled Reports',
  'LBL_NEW_FORM_TITLE' => 'New Scheduled Reports',
  'LBL_SCHEDULE' => 'Schedule',
  'LBL_AOR_REPORT_NAME' => '레포트',
  'LBL_SCHEDULED_REPORTS_INFORMATION' => 'Scheduled Reports',
  'LBL_LAST_RUN' => 'Last run',
  'LBL_STATUS' => '상태',
);