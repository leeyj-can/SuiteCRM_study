<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_MODULE_NAME' => '타켓리스트',
  'LBL_MODULE_ID'   => '타켓리스트',
  'LBL_MODULE_TITLE' => 'Target Lists: Home',
  'LBL_SEARCH_FORM_TITLE' => 'Target Lists Search',
  'LBL_LIST_FORM_TITLE' => '타켓리스트',
  'LBL_PROSPECT_LIST_NAME' => '이름',
  'LBL_NAME' => '이름',
  'LBL_ENTRIES' => 'Total Entries',
  'LBL_LIST_PROSPECT_LIST_NAME' => '타켓리스트',
  'LBL_LIST_ENTRIES' => 'Targets in List',
  'LBL_LIST_DESCRIPTION' => '상세',
  'LBL_LIST_TYPE_NO' => '종류',
  'LBL_LIST_END_DATE' => '종료일',
  'LBL_DATE_ENTERED' => '입력일',
  'LBL_MARKETING_ID' => 'Marketing Id',
  'LBL_DATE_MODIFIED' => '편집일',
  'LBL_MODIFIED' => '편집자',
  'LBL_CREATED' => '등록자',
  'LBL_TEAM' => 'Team',
  'LBL_ASSIGNED_TO' => '할당유저',
  'LBL_DESCRIPTION' => '상세',
  'LNK_NEW_CAMPAIGN' => 'Create Campaign',
  'LNK_CAMPAIGN_LIST' => '캠페인',
  'LNK_NEW_PROSPECT_LIST' => '타켓리스트작성',
  'LNK_PROSPECT_LIST_LIST' => '타켓리스트',
  'LBL_MODIFIED_BY' => '편집자',
  'LBL_CREATED_BY' => '등록자',
  'LBL_DATE_CREATED' => 'Created date',
  'LBL_DATE_LAST_MODIFIED' => 'Modified date',
  'LNK_NEW_PROSPECT' => '타켓작성',
  'LNK_PROSPECT_LIST' => '타켓',

  'LBL_PROSPECT_LISTS_SUBPANEL_TITLE' => '타켓리스트',
  'LBL_CONTACTS_SUBPANEL_TITLE' => '거래담당자',
  'LBL_LEADS_SUBPANEL_TITLE' => '리드',
  'LBL_PROSPECTS_SUBPANEL_TITLE'=>'타켓',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => '거래처',
  'LBL_COPY_PREFIX' =>'Copy of',
  'LBL_USERS_SUBPANEL_TITLE' =>'유저:',
  'LBL_TYPE' => '종류',
  'LBL_LIST_TYPE' => '종류',
  'LBL_LIST_TYPE_LIST_NAME'=>'종류',
  'LBL_NEW_FORM_TITLE'=>'New Target List',
  'LBL_MARKETING_NAME'=>'Marketing Name',
  'LBL_MARKETING_MESSAGE'=>'Email Marketing Message',
  'LBL_DOMAIN_NAME'=>'Domain Name',
  'LBL_DOMAIN'=>'No emails to Domain',
  'LBL_LIST_PROSPECTLIST_NAME'=>'이름',
	'LBL_MORE_DETAIL' => 'More Detail' /*for 508 compliance fix*/,
);


?>
