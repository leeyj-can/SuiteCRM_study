<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
	'LBL_LIST_ID'=>'Prospect List ID',
	'LBL_ID' => 'ID',
	'LBL_TARGET_TRACKER_KEY'=>'Target Tracker Key',
	'LBL_TARGET_ID'=>'Target ID',
	'LBL_TARGET_TYPE'=>'Target Type',
	'LBL_ACTIVITY_TYPE' =>'Activity Type',
	'LBL_ACTIVITY_DATE' => 'Activity Date',
	'LBL_RELATED_ID' => 'Related Id',
	'LBL_RELATED_TYPE'=>'Related Type',
	'LBL_DELETED' => '삭제',
	'LBL_MODULE_NAME' =>'Campaign Log',
	'LBL_LIST_RECIPIENT_EMAIL'=>'Recipient Email',
	'LBL_LIST_RECIPIENT_NAME'=>'Recipient Name',
	'LBL_ARCHIVED'=>'저장',
	'LBL_HITS'=>'Hits',
	
	'LBL_CAMPAIGN_NAME' => '명칭:',
  	'LBL_CAMPAIGN' => '캠페인:',
  	'LBL_NAME' => '명칭: ',
  	'LBL_INVITEE' => '거래담당자',
  	'LBL_LIST_CAMPAIGN_NAME' => '캠페인',
  	'LBL_LIST_STATUS' => '상태',
  	'LBL_LIST_TYPE' => '종류',
  	'LBL_LIST_END_DATE' => '종료일',
  	'LBL_DATE_ENTERED' => 'Date Entered',
  	'LBL_DATE_MODIFIED' => '편집일',
  	'LBL_MODIFIED' => 'Modified By: ',
  	'LBL_CREATED' => 'Created By: ',
  	'LBL_TEAM' => '부서: ',
  	'LBL_ASSIGNED_TO' => 'Assigned To: ',
  	'LBL_CAMPAIGN_START_DATE' => '시작일: ',
  	'LBL_CAMPAIGN_END_DATE' => '종료일: ',
  	'LBL_CAMPAIGN_STATUS' => '상태: ',
  	'LBL_CAMPAIGN_BUDGET' => '예산: ',
	'LBL_CAMPAIGN_EXPECTED_COST' => '예상경비: ',
  	'LBL_CAMPAIGN_ACTUAL_COST' => '실질경비: ',
  	'LBL_CAMPAIGN_EXPECTED_REVENUE' => '예상매출: ',
  	'LBL_CAMPAIGN_TYPE' => '종류: ',
	'LBL_CAMPAIGN_OBJECTIVE' => '목적: ',
  	'LBL_CAMPAIGN_CONTENT' => '상세: ',
    'LBL_CREATED_LEAD' => 'Created Lead',
    'LBL_CREATED_CONTACT' => 'Created Contact',
    'LBL_CREATED_OPPORTUNITY' => 'Created Opportunity',
    'LBL_TARGETED_USER' => 'Targeted User',
    'LBL_SENT_EMAIL' => 'Sent Email',
  	'LBL_LIST_FORM_TITLE'=> 'Targeted Campaigns',	
  	'LBL_LIST_ACTIVITY_DATE'=>'Activity Date',
  	'LBL_LIST_CAMPAIGN_OBJECTIVE'=>'Campaign Objective',
  	'LBL_RELATED'=>'Related',
  	'LBL_CLICKED_URL_KEY'=>'Clicked URL Key',
  	'LBL_URL_CLICKED'=>'URL Clicked',
  	'LBL_MORE_INFO'=>'More Information',
    
    'LBL_CAMPAIGNS' => '캠페인',
	'LBL_LIST_MARKETING_NAME' => 'Marketing Id',
);
?>