<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
	'ERR_DELETE_RECORD' => 'You must specify a record number to delete the account.',
	'LBL_ACCOUNT_ID' => '거래처ID:',
	'LBL_CASE_ID' => '사례ID:',
	'LBL_CLOSE' => '종료:',
	'LBL_COLON' => ':',
	'LBL_CONTACT_ID' => '거래담당자ID:',
	'LBL_CONTACT_NAME' => '거래담당자:',
	'LBL_DEFAULT_SUBPANEL_TITLE' => '노트',
	'LBL_DESCRIPTION' => '상세',
	'LBL_EMAIL_ADDRESS' => 'E메일주소:',
    'LBL_EMAIL_ATTACHMENT' => 'Email Attachment',
	'LBL_FILE_MIME_TYPE' => 'Mime Type',
	'LBL_FILE_URL' => 'File URL',
	'LBL_FILENAME' => '첨부파일:',
	'LBL_LEAD_ID' => '리드ID:',
	'LBL_LIST_CONTACT_NAME' => '거래담당자',
	'LBL_LIST_DATE_MODIFIED' => '최종변경일',
	'LBL_LIST_FILENAME' => '첨부파일',
	'LBL_LIST_FORM_TITLE' => '노트 리스트',
	'LBL_LIST_RELATED_TO' => '관련',
	'LBL_LIST_SUBJECT' => '제목',
	'LBL_LIST_STATUS' => '상태',
	'LBL_LIST_CONTACT' => '거래담당자',
	'LBL_MODULE_NAME' => '노트',
	'LBL_MODULE_TITLE' => '노트: 홈',
	'LBL_NEW_FORM_TITLE' => '노트 작성',
	'LBL_NOTE_STATUS' => '노트',
	'LBL_NOTE_SUBJECT' => '노트 제목:',
	'LBL_NOTES_SUBPANEL_TITLE' => '첨부파일',
	'LBL_NOTE' => '노트:',
	'LBL_OPPORTUNITY_ID' => '안건 ID:',
	'LBL_PARENT_ID' => '패런트ID:',
	'LBL_PARENT_TYPE' => '패런트종류',
	'LBL_PHONE' => '전화:',
	'LBL_PORTAL_FLAG' => '포탈에표시?',
	'LBL_EMBED_FLAG' => 'Embed in email?',
	'LBL_PRODUCT_ID' => '제품 ID:',
	'LBL_QUOTE_ID' => '견적 ID:',
	'LBL_RELATED_TO' => '관련처:',
	'LBL_SEARCH_FORM_TITLE' => '노트 검색',
	'LBL_STATUS' => '상태',
	'LBL_SUBJECT' => '제목:',
	'LNK_IMPORT_NOTES' => '노트읽어들이기',
	'LNK_NEW_NOTE' => '노트 작성',
	'LNK_NOTE_LIST' => '노트',
	'LBL_MEMBER_OF' => '멤버:',
	'LBL_LIST_ASSIGNED_TO_NAME' => '할당유저',
    'LBL_REMOVING_ATTACHMENT'=>'Removing attachment...',
    'ERR_REMOVING_ATTACHMENT'=>'Failed to remove attachment...',
    'LBL_CREATED_BY'=>'Created By',
    'LBL_MODIFIED_BY'=>'Modified By',
    'LBL_SEND_ANYWAYS'=> 'This email has no subject.  Send/save anyway?',
	'LBL_LIST_EDIT_BUTTON' => '편집',
	'LBL_ACTIVITIES_REPORTS' => 'Activities Report',
	'LBL_PANEL_DETAILS' => 'Details',
	'LBL_NOTE_INFORMATION' => '리드 정보',
	'LBL_MY_NOTES_DASHLETNAME' => 'My Notes',
	'LBL_EDITLAYOUT' => 'Edit Layout' /*for 508 compliance fix*/,
    //For export labels
	'LBL_FIRST_NAME' => '이름(명):',
    'LBL_LAST_NAME' => '이름(명)',
    'LBL_EXPORT_PARENT_TYPE' => 'Related To Module',
    'LBL_EXPORT_PARENT_ID' => 'Related To ID',
    'LBL_DATE_ENTERED' => '입력일',
    'LBL_DATE_MODIFIED' => '편집일',
    'LBL_DELETED' => '삭제',
);

?>
