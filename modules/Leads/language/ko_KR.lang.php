<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

* Description:  Defines the English language pack for the base application.
* Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
* All Rights Reserved.
* Contributor(s): ______________________________________..
********************************************************************************/

$mod_strings = array (
    //DON'T CONVERT THESE THEY ARE MAPPINGS
    'db_last_name' => 'LBL_LIST_LAST_NAME',
    'db_first_name' => 'LBL_LIST_FIRST_NAME',
    'db_title' => 'LBL_LIST_TITLE',
    'db_email1' => 'LBL_LIST_EMAIL_ADDRESS',
    'db_account_name' => 'LBL_LIST_ACCOUNT_NAME',
    'db_email2' => 'LBL_LIST_EMAIL_ADDRESS',

    //END DON'T CONVERT
    'ERR_DELETE_RECORD' => 'en_us A record number must be specified to delete the lead.',
    'LBL_ACCOUNT_DESCRIPTION'=> '거래처 상세',
    'LBL_ACCOUNT_ID'=>'거래처 ID',
    'LBL_ACCOUNT_NAME' => '거래처명:',
    'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'영업활동',
    'LBL_ADD_BUSINESSCARD' => '명함추가',
    'LBL_ADDRESS_INFORMATION' => '주소 정보',
    'LBL_ALT_ADDRESS_CITY' => '부주소 시구군',
    'LBL_ALT_ADDRESS_COUNTRY' => '부주소 국가',
    'LBL_ALT_ADDRESS_POSTALCODE' => '부주소 우편코드',
    'LBL_ALT_ADDRESS_STATE' => '부주소 읍면동',
    'LBL_ALT_ADDRESS_STREET_2' => '부주소 주소2',
    'LBL_ALT_ADDRESS_STREET_3' => '부주소 주소3',
    'LBL_ALT_ADDRESS_STREET' => '부주소 주소',
    'LBL_ALTERNATE_ADDRESS' => '연락주소:',
    'LBL_ALT_ADDRESS' => '연락주소:',
    'LBL_ANY_ADDRESS' => '연락주소:',
    'LBL_ANY_EMAIL' => 'E메일:',
    'LBL_ANY_PHONE' => '전화:',
    'LBL_ASSIGNED_TO_NAME' => '할당유저',
    'LBL_ASSIGNED_TO_ID' => 'Assigned User:',
    'LBL_BACKTOLEADS' => '리드로돌아감',
    'LBL_BUSINESSCARD' => '리드변환',
    'LBL_CITY' => '시구군:',
    'LBL_CONTACT_ID' => '거래담당자 ID',
    'LBL_CONTACT_INFORMATION' => '리드 정보',
    'LBL_CONTACT_NAME' => '리드 정보:',
    'LBL_CONTACT_OPP_FORM_TITLE' => '리드-안건:',
    'LBL_CONTACT_ROLE' => '역활:',
    'LBL_CONTACT' => '리드:',
    'LBL_CONVERTED_ACCOUNT'=>'거래처 변환:',
    'LBL_CONVERTED_CONTACT' => '거래담당자 변환:',
    'LBL_CONVERTED_OPP'=>'안건 변환:',
    'LBL_CONVERTED'=> '변환',
    'LBL_CONVERTLEAD_BUTTON_KEY' => 'V',
    'LBL_CONVERTLEAD_TITLE' => '리드변환[Alt+V]',
    'LBL_CONVERTLEAD' => '리드변환',
    'LBL_CONVERTLEAD_WARNING' => 'Warning: The status of the Lead you are about to convert is "Converted". Contact and/or Account records may already have been created from the Lead. If you wish to continue with converting the Lead, click Save. To go back to the Lead without converting it, click Cancel.',
    'LBL_CONVERTLEAD_WARNING_INTO_RECORD' => ' Possible Contact: ',
    'LBL_COUNTRY' => '국가:',
    'LBL_CREATED_NEW' => 'Created a new',
	'LBL_CREATED_ACCOUNT' => '신규거래처작성',
    'LBL_CREATED_CALL' => '신규 콜작성',
    'LBL_CREATED_CONTACT' => '신규 거래담당자 작성',
    'LBL_CREATED_MEETING' => '신규 미팅 작성',
    'LBL_CREATED_OPPORTUNITY' => '신규 안건 작성',
    'LBL_DEFAULT_SUBPANEL_TITLE' => '리드',
    'LBL_DEPARTMENT' => '부문:',
    'LBL_DESCRIPTION_INFORMATION' => '상세 정보',
    'LBL_DESCRIPTION' => '상세:',
    'LBL_DO_NOT_CALL' => '전화불가:',
    'LBL_DUPLICATE' => '같은종류의 리드',
    'LBL_EMAIL_ADDRESS' => 'E메일:',
    'LBL_EMAIL_OPT_OUT' => '메일송신:',
    'LBL_EXISTING_ACCOUNT' => '기존 거래처 사용',
    'LBL_EXISTING_CONTACT' => '기존 거래담당자 사용',
    'LBL_EXISTING_OPPORTUNITY' => '기존 안건 사용',
    'LBL_FAX_PHONE' => '팩스:',
    'LBL_FIRST_NAME' => '이름(성):',
    'LBL_FULL_NAME' => '이름:',
    'LBL_HISTORY_SUBPANEL_TITLE'=>'이력',
    'LBL_HOME_PHONE' => '자택전화:',
    'LBL_IMPORT_VCARD' => 'vCard에서작성',
    'LBL_VCARD' => 'vCard',
    'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new lead by importing a vCard from your file system.',
    'LBL_INVALID_EMAIL'=>'무효E메일:',
    'LBL_INVITEE' => '직속상관',
    'LBL_LAST_NAME' => '이름(명):',
    'LBL_LEAD_SOURCE_DESCRIPTION' => '리드 소스 상세:',
    'LBL_LEAD_SOURCE' => '리드 소스:',
    'LBL_LIST_ACCEPT_STATUS' => '허가상태',
    'LBL_LIST_ACCOUNT_NAME' => '거래처명',
    'LBL_LIST_CONTACT_NAME' => '리드명',
    'LBL_LIST_CONTACT_ROLE' => '역활',
    'LBL_LIST_DATE_ENTERED' => '입력일',
    'LBL_LIST_EMAIL_ADDRESS' => 'E메일',
    'LBL_LIST_FIRST_NAME' => '이름(성)',
    'LBL_VIEW_FORM_TITLE' => '리드표시',    
    'LBL_LIST_FORM_TITLE' => '리드 리스트',
    'LBL_LIST_LAST_NAME' => '이름(명)',
    'LBL_LIST_LEAD_SOURCE_DESCRIPTION' => '리드 소스 상세',
    'LBL_LIST_LEAD_SOURCE' => '리드 소스',
    'LBL_LIST_MY_LEADS' => 'My 리드 리스트',
    'LBL_LIST_NAME' => '이름',
    'LBL_LIST_PHONE' => '직장 전화',
    'LBL_LIST_REFERED_BY' => '소개자',
    'LBL_LIST_STATUS' => '상태',
    'LBL_LIST_TITLE' => '직위',
    'LBL_MOBILE_PHONE' => '휴대폰:',
    'LBL_MODULE_NAME' => '리드',
    'LBL_MODULE_TITLE' => '리드: 홈',
    'LBL_NAME' => '이름:',
    'LBL_NEW_FORM_TITLE' => '리드 작성',
    'LBL_NEW_PORTAL_PASSWORD' => '신규 포탈패스워드:',
    'LBL_OFFICE_PHONE' => '직장전화:',
    'LBL_OPP_NAME' => '안건명:',
    'LBL_OPPORTUNITY_AMOUNT' => '안건 금액:',
    'LBL_OPPORTUNITY_ID'=>'안건 ID',
    'LBL_OPPORTUNITY_NAME' => '안건명:',
    'LBL_OTHER_EMAIL_ADDRESS' => '연락E메일:',
    'LBL_OTHER_PHONE' => '연락 전화:',
    'LBL_PHONE' => '전화:',
    'LBL_PORTAL_ACTIVE' => '포탈 액티브:',
    'LBL_PORTAL_APP'=> '포탈 어픞리케이션',
    'LBL_PORTAL_INFORMATION' => '포탈 정보',
    'LBL_PORTAL_NAME' => '포탈명:',
    'LBL_PORTAL_PASSWORD_ISSET' => '포탈 패스워드 설정:',
    'LBL_POSTAL_CODE' => '우편번호:',
    'LBL_STREET' => 'Street',
    'LBL_PRIMARY_ADDRESS_CITY' => '시구군',
    'LBL_PRIMARY_ADDRESS_COUNTRY' => '국가',
    'LBL_PRIMARY_ADDRESS_POSTALCODE' => '우편번호',
    'LBL_PRIMARY_ADDRESS_STATE' => '읍면동',
    'LBL_PRIMARY_ADDRESS_STREET_2'=>'주소 2',
    'LBL_PRIMARY_ADDRESS_STREET_3'=>'주소 3',   
    'LBL_PRIMARY_ADDRESS_STREET' => '주소',
    'LBL_PRIMARY_ADDRESS' => '메인주소:',
    'LBL_REFERED_BY' => '소개자:',
    'LBL_REPORTS_TO_ID'=>'직속상사ID',
    'LBL_REPORTS_TO' => '직속상사:',
    'LBL_REPORTS_FROM' => 'Reports From:',
    'LBL_SALUTATION' => '경칭',
    'LBL_MODIFIED'=>'Modified By',
	'LBL_MODIFIED_ID'=>'Modified By Id',
	'LBL_CREATED'=>'Created By',
	'LBL_CREATED_ID'=>'Created By Id',    
    'LBL_SEARCH_FORM_TITLE' => '리드 검색',
    'LBL_SELECT_CHECKED_BUTTON_LABEL' => '체크리드선택',
    'LBL_SELECT_CHECKED_BUTTON_TITLE' => '체크리드선택',
    'LBL_STATE' => '읍면동:',
    'LBL_STATUS_DESCRIPTION' => '상태 상세:',
    'LBL_STATUS' => '상태:',
    'LBL_TITLE' => '직위:',
    'LNK_IMPORT_VCARD' => 'vCard에서 작성',
    'LNK_LEAD_LIST' => '리드',
    'LNK_NEW_ACCOUNT' => '거래처 작성',
    'LNK_NEW_APPOINTMENT' => '약속 작성',
    'LNK_NEW_CONTACT' => '거래담당자 작성',
    'LNK_NEW_LEAD' => '리드 작성',
    'LNK_NEW_NOTE' => '노트 작성',
    'LNK_NEW_TASK' => '타스크작성',
    'LNK_NEW_CASE' => '사례작성',
    'LNK_NEW_CALL' => '콜스케쥴작성',
    'LNK_NEW_MEETING' => '미팅스케쥴작성',
    'LNK_NEW_OPPORTUNITY' => '안건 작성',
	'LNK_SELECT_ACCOUNTS' => ' <b>OR</b> Select Account',
    'LNK_SELECT_CONTACTS' => ' <b>OR</b> Select Contact',
    'NTC_COPY_ALTERNATE_ADDRESS' => 'Copy alternate address to primary address',
    'NTC_COPY_PRIMARY_ADDRESS' => 'Copy primary address to alternate address',
    'NTC_DELETE_CONFIRMATION' => 'Are you sure you want to delete this record?',
    'NTC_OPPORTUNITY_REQUIRES_ACCOUNT' => 'Creating an opportunity requires an account.\n Please either create a new one or select an existing one.',
    'NTC_REMOVE_CONFIRMATION' => 'Are you sure you want to remove this lead from this case?',
    'NTC_REMOVE_DIRECT_REPORT_CONFIRMATION' => 'Are you sure you want to remove this record as a direct report?',
    'LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'=>'캠페인',
    'LBL_TARGET_OF_CAMPAIGNS'=>'성공한캠페인:',
    'LBL_TARGET_BUTTON_LABEL'=>'타켓작성',
    'LBL_TARGET_BUTTON_TITLE'=>'타켓작성',
    'LBL_TARGET_BUTTON_KEY'=>'T',
    'LBL_CAMPAIGN' => '캠페인:',
  	'LBL_LIST_ASSIGNED_TO_NAME' => '할당유저',
    'LBL_PROSPECT_LIST' => 'Prospect List',
    'LBL_CAMPAIGN_LEAD' => '캠페인',
    'LBL_BIRTHDATE' => '생년월일:',
    'LBL_THANKS_FOR_SUBMITTING_LEAD' =>'Thank You For Your Submission.',
    'LBL_SERVER_IS_CURRENTLY_UNAVAILABLE' =>'We are sorry, the server is currently unavailable, please try again later.',
    'LBL_ASSISTANT_PHONE' => '보조전화',
    'LBL_ASSISTANT' => '보조',
    'LBL_REGISTRATION' => 'Registration',
    'LBL_MESSAGE' => 'Please enter your information below. Information and/or an account will be created for you pending approval.',
    'LBL_SAVED' => 'Thank you for registering. Your account will be created and someone will contact you shortly.', 
    'LBL_CLICK_TO_RETURN' => 'Return to Portal',
    'LBL_CREATED_USER' => 'Created User',
    'LBL_MODIFIED_USER' => 'Modified User',
    'LBL_CAMPAIGNS' => '캠페인',
    'LBL_CAMPAIGNS_SUBPANEL_TITLE' => '캠페인',
    'LBL_CONVERT_MODULE_NAME' => 'Module',
    'LBL_CONVERT_REQUIRED' => 'Required',
    'LBL_CONVERT_SELECT' => 'Allow Selection',
    'LBL_CONVERT_COPY' => 'Copy Data',
    'LBL_CONVERT_EDIT' => '편집',
    'LBL_CONVERT_DELETE' => '삭제',
    'LBL_CONVERT_ADD_MODULE' => 'Add Module',
    'LBL_CREATE' => '작성',
    'LBL_SELECT' => ' <b>OR</b> Select',
	'LBL_WEBSITE' => '웹사이트',
	'LNK_IMPORT_LEADS' => 'Import Leads',
	'LBL_NOTICE_OLD_LEAD_CONVERT_OVERRIDE' => 'Notice: The current Convert Lead screen contains custom fields. When you customize the Convert Lead screen in Studio for the first time, you will need to add custom fields to the layout, as necessary. The custom fields will not automatically appear in the layout, as they did previously.',
//Convert lead tooltips
	'LBL_MODULE_TIP' 	=> 'The module to create a new record in.',
	'LBL_REQUIRED_TIP' 	=> 'Required modules must be created or selected before the lead can be converted.',
	'LBL_COPY_TIP'		=> 'If checked, fields from the lead will be copied to fields with the same name in the newly created records.',
	'LBL_SELECTION_TIP' => 'Modules with a relate field in Contacts can be selected rather than created during the convert lead process.',
	'LBL_EDIT_TIP'		=> 'Modify the convert layout for this module.',
	'LBL_DELETE_TIP'	=> 'Remove this module from the convert layout.',

    'LBL_ACTIVITIES_MOVE'   => 'Move Activities to',
    'LBL_ACTIVITIES_COPY'   => 'Copy Activities to',
    'LBL_ACTIVITIES_MOVE_HELP'   => "Select the record to which to move the Lead's activities. Tasks, Calls, Meetings, Notes and Emails will be moved to the selected record(s).",
    'LBL_ACTIVITIES_COPY_HELP'   => "Select the record(s) for which to create copies of the Lead's activities. New Tasks, Calls, Meetings and Notes will be created for each of the selected record(s). Emails will be related to the selected record(s).",
    //For export labels
    'LBL_PHONE_HOME' => 'Phone Home',
    'LBL_PHONE_MOBILE' => '모바일:',
    'LBL_PHONE_WORK' => 'Phone Work',
    'LBL_PHONE_OTHER' => 'Phone Other',
    'LBL_PHONE_FAX' => '팩스:',
    'LBL_CAMPAIGN_ID' => 'Campaign ID',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Assigned User ID',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Modified By ID',
    'LBL_EXPORT_CREATED_BY' => 'Created By ID',
    'LBL_EXPORT_PHONE_MOBILE' => 'Mobile Phone',
    'LBL_EXPORT_EMAIL2'=>'Other Email Address',
	'LBL_EDITLAYOUT' => 'Edit Layout' /*for 508 compliance fix*/,
	'LBL_ENTERDATE' => '작성일' /*for 508 compliance fix*/,
	'LBL_LOADING' => 'Loading' /*for 508 compliance fix*/,
	'LBL_EDIT_INLINE' => '편집' /*for 508 compliance fix*/,
);
?>
