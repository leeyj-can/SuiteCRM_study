<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_MODULE_NAME' => '타스크',
  'LBL_TASK' => '타스크: ',
  'LBL_MODULE_TITLE' => '타스크: 홈',
  'LBL_SEARCH_FORM_TITLE' => '타스크 검색',
  'LBL_LIST_FORM_TITLE' => '타스크 리스트',
  'LBL_NEW_FORM_TITLE' => '타스크 작성',
  'LBL_NEW_FORM_SUBJECT' => '제목:',
  'LBL_NEW_FORM_DUE_DATE' => '종료일:',
  'LBL_NEW_FORM_DUE_TIME' => '종료시간:',
  'LBL_NEW_TIME_FORMAT' => '(24:00)',
  'LBL_LIST_CLOSE' => '종료',
  'LBL_LIST_SUBJECT' => '제목',
  'LBL_LIST_CONTACT' => '거래담당자',
  'LBL_LIST_PRIORITY' => '우선순위',
  'LBL_LIST_RELATED_TO' => '관련처',
  'LBL_LIST_DUE_DATE' => '종료일',
  'LBL_LIST_DUE_TIME' => '종료시간',
  'LBL_SUBJECT' => '제목:',
  'LBL_STATUS' => '상태:',
  'LBL_DUE_DATE' => '종료일:',
  'LBL_DUE_TIME' => '종료시간:',
  'LBL_PRIORITY' => '우선순위:',
  'LBL_COLON' => ':',
  'LBL_DUE_DATE_AND_TIME' => '종료일시:',
  'LBL_START_DATE_AND_TIME' => '시작일시:',
  'LBL_START_DATE' => '시작일:',
  'LBL_LIST_START_DATE' => '시작일',
  'LBL_START_TIME' => '시작시간:',
  'LBL_LIST_START_TIME' => '시작시간',
  'DATE_FORMAT' => '(yyyy-mm-dd)',
  'LBL_NONE' => '미사용',
  'LBL_CONTACT' => '거래담당자:',
  'LBL_EMAIL_ADDRESS' => 'E메일:',
  'LBL_PHONE' => '전화:',
  'LBL_EMAIL' => 'E메일:',
  'LBL_DESCRIPTION_INFORMATION' => '상세정보',
  'LBL_DESCRIPTION' => '상세:',
  'LBL_NAME' => '명칭:',
  'LBL_CONTACT_NAME' => '거래처담당자: ',
  'LBL_LIST_COMPLETE' => '완료:',
  'LBL_LIST_STATUS' => '상태',
  'LBL_DATE_DUE_FLAG' => '종료일',
  'LBL_DATE_START_FLAG' => '시작일',
  'ERR_DELETE_RECORD' => 'You must specify a record number to delete the contact.',
  'ERR_INVALID_HOUR' => 'Please enter an hour between 0 and 24',
  'LBL_DEFAULT_PRIORITY' => '중',
  'LBL_LIST_MY_TASKS' => 'My 오픈 타스크',
  'LNK_NEW_TASK' => '타스크 작성',
  'LNK_TASK_LIST' => '타스크리스트',
  'LNK_IMPORT_TASKS' => 'Import Tasks',
  'LBL_CONTACT_FIRST_NAME'=>'거래담당자 이름(명)',
  'LBL_CONTACT_LAST_NAME'=>'거래담당자 이름(성)',
  'LBL_LIST_ASSIGNED_TO_NAME' => '할당유저',
  'LBL_ASSIGNED_TO_NAME'=>'할당유저:',
  'LBL_LIST_DATE_MODIFIED' => '편집일',
  'LBL_CONTACT_ID' => '거래담당자ID:',
  'LBL_PARENT_ID' => '패런트ID:',
  'LBL_CONTACT_PHONE' => 'Contact Phone:',
  'LBL_PARENT_NAME' => 'Parent Type:',
  'LBL_ACTIVITIES_REPORTS' => 'Activities Report',
  'LBL_TASK_INFORMATION' => 'Task Overview',
  'LBL_EDITLAYOUT' => 'Edit Layout' /*for 508 compliance fix*/,
  'LBL_HISTORY_SUBPANEL_TITLE' => '노트:',
  //For export labels
  'LBL_DATE_DUE' => '만료일',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
  'LBL_EXPORT_ASSIGNED_USER_ID' => 'Assigned User ID',
  'LBL_EXPORT_MODIFIED_USER_ID' => 'Modified By ID',
  'LBL_EXPORT_CREATED_BY' => 'Created By ID',
  'LBL_EXPORT_PARENT_TYPE' => 'Related To Module',
  'LBL_EXPORT_PARENT_ID' => 'Related To ID',
  'LBL_RELATED_TO' => 'Related to:',
);


?>
