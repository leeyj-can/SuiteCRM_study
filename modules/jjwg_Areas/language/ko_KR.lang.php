<?php

$mod_strings['LBL_ASSIGNED_TO_ID'] = 'Assigned User Id';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = '유저';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_DATE_ENTERED'] = '입력일';
$mod_strings['LBL_DATE_MODIFIED'] = '편집일';
$mod_strings['LBL_MODIFIED'] = 'Modified By';
$mod_strings['LBL_MODIFIED_ID'] = 'Modified By Id';
$mod_strings['LBL_MODIFIED_NAME'] = 'Modified By Name';
$mod_strings['LBL_CREATED'] = 'Created By';
$mod_strings['LBL_CREATED_ID'] = 'Created By Id';
$mod_strings['LBL_DESCRIPTION'] = '상세';
$mod_strings['LBL_DELETED'] = '삭제';
$mod_strings['LBL_NAME'] = '이름';
$mod_strings['LBL_CREATED_USER'] = '작성자';
$mod_strings['LBL_MODIFIED_USER'] = '변경유저';
$mod_strings['LBL_LIST_NAME'] = '이름';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Areas List';
$mod_strings['LBL_MODULE_NAME'] = 'Areas';
$mod_strings['LBL_MODULE_TITLE'] = 'Areas';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Areas';
$mod_strings['LNK_NEW_RECORD'] = 'Create Areas';
$mod_strings['LNK_LIST'] = 'View Areas';
$mod_strings['LNK_IMPORT_JJWG_AREAS'] = 'Import Areas';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Areas';
$mod_strings['LBL_HISTORY_SUBPANEL_TITLE'] = 'View History';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = '영업활동';
$mod_strings['LBL_JJWG_AREAS_SUBPANEL_TITLE'] = 'Areas';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Areas';
$mod_strings['LBL_CITY'] = '시구군:';
$mod_strings['LBL_STATE'] = '읍면동:';
$mod_strings['LBL_COUNTRY'] = '나라:';
$mod_strings['LBL_COORDINATES'] = 'Coordinates';
$mod_strings['LBL_LIST_ASSIGNED_USER'] = '유저';
$mod_strings['LBL_AREA_MAP'] = 'Area Map';

$mod_strings['LBL_AREA_EDIT_TITLE'] = 'Area Creation Instructions:';
$mod_strings['LBL_AREA_EDIT_DESC_1'] = 'Left click on the map, in a clockwise motion, to create marker points for the area.';
$mod_strings['LBL_AREA_EDIT_DESC_2'] = 'Click on the first marker point to close the polygon area.';
$mod_strings['LBL_AREA_EDIT_RESET'] = 'Reset';
$mod_strings['LBL_AREA_EDIT_USE_AREA_COORDINATES'] = 'Use Area Coordinates';
$mod_strings['LBL_AREA_EDIT_COORDINATE_RESULTS'] = 'Coordinate Results (lng,lat,elv):';
