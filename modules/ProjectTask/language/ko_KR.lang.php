<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/




$mod_strings = array (
	'LBL_MODULE_NAME' => '프로젝트 타스크',
	'LBL_MODULE_TITLE' => '프로젝트 타스크: 홈',
	'LBL_SEARCH_FORM_TITLE' => '프로젝트 타스크 검색',
	'LBL_LIST_FORM_TITLE'=> '프로젝트 타스크 리스트',
    'LBL_EDIT_TASK_IN_GRID_TITLE'=> 'Edit Task In Grid',    
	
	'LBL_ID' => 'Id:',
    'LBL_PROJECT_TASK_ID' => 'Project Task Id:',
    'LBL_PROJECT_ID' => 'Project Id:',
	'LBL_DATE_ENTERED' => '등록일:',
	'LBL_DATE_MODIFIED' => '변경일:',
	'LBL_ASSIGNED_USER_ID' => '할당:',
	'LBL_MODIFIED_USER_ID' => '변경유저 Id:',
	'LBL_CREATED_BY' => '작성자:',
	'LBL_TEAM_ID' => '부서:',
	'LBL_NAME' => '명칭:',
	'LBL_STATUS' => '상태:',
	'LBL_DATE_DUE' => '만료일:',
	'LBL_TIME_DUE' => '만료시간:',
    'LBL_RESOURCE' => 'Resource:',
    'LBL_PREDECESSORS' => 'Predecessors:',
	'LBL_DATE_START' => '시작일:',
    'LBL_DATE_FINISH' => 'Finish Date:',    
	'LBL_TIME_START' => '시작시간:',
    'LBL_TIME_FINISH' => 'Finish Time:',
    'LBL_DURATION' => '시간:',
    'LBL_DURATION_UNIT' => 'Duration Unit:',
    'LBL_ACTUAL_DURATION' => 'Actual Duration:',
	'LBL_PARENT_ID' => '프로젝트:',
    'LBL_PARENT_TASK_ID' => 'Parent Task Id:',    
    'LBL_PERCENT_COMPLETE' => '진척율 (%):',
	'LBL_PRIORITY' => '우선순위:',
	'LBL_DESCRIPTION' => '상세:',
	'LBL_ORDER_NUMBER' => '주문:',
	'LBL_TASK_NUMBER' => '타스크번호:',
    'LBL_TASK_ID' => 'Task ID:',
	'LBL_DEPENDS_ON_ID' => '의존처:',
	'LBL_MILESTONE_FLAG' => '중요:',
	'LBL_ESTIMATED_EFFORT' => '견적시간(시간):',
	'LBL_ACTUAL_EFFORT' => '실적시간(시간):',
	'LBL_UTILIZATION' => '부하률 (%):',
	'LBL_DELETED' => '삭제:',

	'LBL_LIST_ORDER_NUMBER' => '주문',
	'LBL_LIST_NAME' => '명칭',
    'LBL_LIST_DAYS' => 'days',
	'LBL_LIST_PARENT_NAME' => '프로젝트',
	'LBL_LIST_PERCENT_COMPLETE' => '진척률(%)',
	'LBL_LIST_STATUS' => '상태',
    'LBL_LIST_DURATION' => '시간',
    'LBL_LIST_ACTUAL_DURATION' => 'Actual Duration',
	'LBL_LIST_ASSIGNED_USER_ID' => '할당',
	'LBL_LIST_DATE_DUE' => '만료일',
	'LBL_LIST_DATE_START' => '시작일',
    'LBL_LIST_DATE_FINISH' => 'Finish Date',    
	'LBL_LIST_PRIORITY' => '우선순위',
	'LBL_LIST_CLOSE' => '종료',
	'LBL_PROJECT_NAME' => '프로젝트명',

	'LNK_NEW_PROJECT'	=> '프로젝트 작성',
	'LNK_PROJECT_LIST'	=> '프로젝트리스트',
	'LNK_NEW_PROJECT_TASK'	=> '프로젝트 타스크 작성',
	'LNK_PROJECT_TASK_LIST'	=> '프로젝트 타스크 리스트',
	
	'LBL_LIST_MY_PROJECT_TASKS' => 'My Project Tasks',
	'LBL_DEFAULT_SUBPANEL_TITLE' => '프로젝트 타스크s',
	'LBL_NEW_FORM_TITLE' => '프로젝트 타스크 작성',

	'LBL_ACTIVITIES_TITLE'=>'영업활동',
	'LBL_HISTORY_TITLE'=>'이력',
	'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'영업활동',
	'LBL_HISTORY_SUBPANEL_TITLE'=>'이력', 
	'DATE_JS_ERROR' => 'Please enter a date corresponding to the time entered',

    'LBL_ASSIGNED_USER_NAME' => '할당',
    'LBL_PARENT_NAME' => '프로젝트명',
    'LBL_LIST_PROJECT_NAME' => '프로젝트',
	'LBL_EDITLAYOUT' => 'Edit Layout' /*for 508 compliance fix*/,
    'LBL_PANEL_TIMELINE' => 'Timeline',
);
?>