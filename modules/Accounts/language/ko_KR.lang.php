<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
	// DON'T CONVERT THESE THEY ARE MAPPINGS
	'db_name' => 'LBL_LIST_ACCOUNT_NAME',
	'db_website' => 'LBL_LIST_WEBSITE',
	'db_billing_address_city' => 'LBL_LIST_CITY',
	// END DON'T CONVERT
    'LBL_DOCUMENTS_SUBPANEL_TITLE' => '자료관리',
	// Dashlet Categories
	'LBL_CHARTS'    => '챠트',
	'LBL_DEFAULT' => '화면',
	'LBL_MISC'    => '기타',
	'LBL_UTILS'    => '유틀리티',
	// END Dashlet Categories

	'ACCOUNT_REMOVE_PROJECT_CONFIRM' => 'Are you sure you want to remove this account from the project?',
	'ERR_DELETE_RECORD' => 'You must specify a record number in order to delete the account.',
	'LBL_ACCOUNT_INFORMATION' => '거래처 정보',
	'LBL_ACCOUNT_NAME' => '거래처명:',
	'LBL_ACCOUNT' => '거래처:',
	'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'영업활동',
	'LBL_ADDRESS_INFORMATION' => '주소 정보',
	'LBL_ANNUAL_REVENUE' => '년간매출:',
	'LBL_ANY_ADDRESS' => '연락주소:',
	'LBL_ANY_EMAIL' => '연락메일:',
	'LBL_ANY_PHONE' => '연락전화:',
	'LBL_ASSIGNED_TO_NAME' => '할당유저명:',
	'LBL_ASSIGNED_TO_ID' => 'Assigned User:',
	'LBL_BILLING_ADDRESS_CITY' => '청구처 시구군:',
	'LBL_BILLING_ADDRESS_COUNTRY' => '청구처 국가:',
	'LBL_BILLING_ADDRESS_POSTALCODE' => '청구처 우편번호:',
	'LBL_BILLING_ADDRESS_STATE' => '청구처 읍면동:',
	'LBL_BILLING_ADDRESS_STREET_2' =>'청구처 주소2',
	'LBL_BILLING_ADDRESS_STREET_3' =>'청구처 주소3',
	'LBL_BILLING_ADDRESS_STREET_4' =>'청구처 주소4',
	'LBL_BILLING_ADDRESS_STREET' => '청구처 주소1:',
	'LBL_BILLING_ADDRESS' => '청구처 주소:',
	'LBL_BUG_FORM_TITLE' => '거래처',
	'LBL_BUGS_SUBPANEL_TITLE' => '결함정보',
	'LBL_CALLS_SUBPANEL_TITLE' => '콜',
	'LBL_CAMPAIGN_ID' => 'Campaign ID',
	'LBL_CASES_SUBPANEL_TITLE' => '사례',
	'LBL_CITY' => '시구군:',
	'LBL_CONTACTS_SUBPANEL_TITLE' => '거래담당자',
	'LBL_COUNTRY' => '국가:',
	'LBL_DATE_ENTERED' => '작성일:',
	'LBL_DATE_MODIFIED' => '최종변경일:',
	'LBL_MODIFIED_ID'=>'Modified By Id',
	'LBL_DEFAULT_SUBPANEL_TITLE' => '거래처',
	'LBL_DESCRIPTION_INFORMATION' => '상세 정보',
	'LBL_DESCRIPTION' => '상세:',
	'LBL_DUPLICATE' => 'Possible Duplicate Account',
	'LBL_EMAIL' => 'E메일:',
	'LBL_EMAIL_OPT_OUT' => '메일송신:',
	'LBL_EMAIL_ADDRESSES' => 'Email Addresses',
	'LBL_EMPLOYEES' => '종업원:',
	'LBL_FAX' => '팩스:',
	'LBL_HISTORY_SUBPANEL_TITLE'=>'이력',
	'LBL_HOMEPAGE_TITLE' => 'My Accounts',
	'LBL_INDUSTRY' => '업종:',
	'LBL_INVALID_EMAIL'=>'무효E메일:',
	'LBL_INVITEE' => '거래담당자',
	'LBL_LEADS_SUBPANEL_TITLE' => '리드',
	'LBL_LIST_ACCOUNT_NAME' => '이름',
	'LBL_LIST_CITY' => '시구군',
	'LBL_LIST_CONTACT_NAME' => '거래처 담당자명',
	'LBL_LIST_EMAIL_ADDRESS' => 'E메일 리스트',
	'LBL_LIST_FORM_TITLE' => 'Account List',
	'LBL_LIST_PHONE' => '전화',
	'LBL_LIST_STATE' => '읍면동',
	'LBL_LIST_WEBSITE' => '웹사이트',
	'LBL_MEETINGS_SUBPANEL_TITLE' => '미팅',
	'LBL_MEMBER_OF' => '멤버의:',
	'LBL_MEMBER_ORG_FORM_TITLE' => '멤버조직',
	'LBL_MEMBER_ORG_SUBPANEL_TITLE'=>'멤버조직',
	'LBL_MODULE_NAME' => '거래처',
	'LBL_MODULE_TITLE' => 'Accounts: Home',
	'LBL_MODULE_ID'=> '거래처',
	'LBL_NAME'=>'이름:',
	'LBL_NEW_FORM_TITLE' => '신규거래처작성',
	'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => '안건',
	'LBL_OTHER_EMAIL_ADDRESS' => '연락E메일:',
	'LBL_OTHER_PHONE' => '연락전화:',
	'LBL_OWNERSHIP' => '소유자:',
	'LBL_PARENT_ACCOUNT_ID' => 'Parent Account ID',
	'LBL_PHONE_ALT' => '연락전화:',
	'LBL_PHONE_FAX' => '팩스:',
	'LBL_PHONE_OFFICE' => '직장전화:',
	'LBL_PHONE' => '전화:',
	'LBL_POSTAL_CODE' => '우편번호:',
	'LBL_PRODUCTS_TITLE'=>'제품',
	'LBL_PROJECTS_SUBPANEL_TITLE' => '제품',
	'LBL_PUSH_BILLING' => 'Push Billing',
	'LBL_PUSH_CONTACTS_BUTTON_LABEL' => '거래담당자 복사',
	'LBL_PUSH_CONTACTS_BUTTON_TITLE' => '복사...',
	'LBL_PUSH_SHIPPING' => 'Push Shipping',
	'LBL_RATING' => '업계순위:',
	'LBL_SAVE_ACCOUNT' => '거래처저장',
	'LBL_SEARCH_FORM_TITLE' => 'Account Search',
	'LBL_SHIPPING_ADDRESS_CITY' => '배송처 시구군:',
	'LBL_SHIPPING_ADDRESS_COUNTRY' => '배송처 국가:',
	'LBL_SHIPPING_ADDRESS_POSTALCODE' => '배송처 우편번호:',
	'LBL_SHIPPING_ADDRESS_STATE' => '배송처 주소:',
	'LBL_SHIPPING_ADDRESS_STREET_2' => '배송처 주소2',
	'LBL_SHIPPING_ADDRESS_STREET_3' => '배송처 주소3',
	'LBL_SHIPPING_ADDRESS_STREET_4' => '배송처 주소4',
	'LBL_SHIPPING_ADDRESS_STREET' => '배송처 주소:',
	'LBL_SHIPPING_ADDRESS' => '배송처 주소:',
	'LBL_SIC_CODE' => '업종코드:',
	'LBL_STATE' => '읍면동:',
	'LBL_TASKS_SUBPANEL_TITLE' => '타스크',
	'LBL_TEAMS_LINK'=>'부서',
	'LBL_TICKER_SYMBOL' => '증권코드:',
	'LBL_TYPE' => '업태:',
	'LBL_USERS_ASSIGNED_LINK'=>'할당유저',
	'LBL_USERS_CREATED_LINK'=>'작성유저',
	'LBL_USERS_MODIFIED_LINK'=>'변경유저',
	'LBL_VIEW_FORM_TITLE' => 'Account View',
	'LBL_WEBSITE' => '웹사이트:',
	'LBL_CREATED_ID'=>'Created By Id',
	'LBL_CAMPAIGNS' =>'캠페인',
	'LNK_ACCOUNT_LIST' => '거래처',
	'LNK_NEW_ACCOUNT' => '거래처 작성',
	'LNK_IMPORT_ACCOUNTS' => 'Import Accounts',
	'MSG_DUPLICATE' => 'The account record you are about to create might be a duplicate of an account record that already exists. Account records containing similar names are listed below.<br>Click Create Account to continue creating this new account, or select an existing account listed below.',
	'MSG_SHOW_DUPLICATES' => 'The account record you are about to create might be a duplicate of an account record that already exists. Account records containing similar names are listed below.<br>Click Save to continue creating this new account, or click Cancel to return to the module without creating the account.',
	'NTC_COPY_BILLING_ADDRESS' => '청구처 주소를 배송처 주소로 복사',
	'NTC_COPY_BILLING_ADDRESS2' => 'Copy to shipping',
	'NTC_COPY_SHIPPING_ADDRESS' => '배송처 주소를 청구처 주소로 복사',
	'NTC_COPY_SHIPPING_ADDRESS2' => 'Copy to billing',
	'NTC_DELETE_CONFIRMATION' => 'Are you sure you want to delete this record?',
	'NTC_REMOVE_ACCOUNT_CONFIRMATION' => 'Are you sure you want to remove this record?',
	'NTC_REMOVE_MEMBER_ORG_CONFIRMATION' => 'Are you sure you want to remove this record as a member organization?',
	'LBL_ASSIGNED_USER_NAME' => '할당유저:',
    'LBL_PROSPECT_LIST' => 'Prospect List',
    'LBL_ACCOUNTS_SUBPANEL_TITLE'=>'거래처',
    'LBL_PROJECT_SUBPANEL_TITLE' => '프로젝트',
	'LBL_COPY' => 'Copy' /*for 508 compliance fix*/,
    //For export labels
    'LBL_ACCOUNT_TYPE' => 'Account Type',
    'LBL_PARENT_ID' => 'Parent ID',
    'LBL_PHONE_ALTERNATE' => 'Phone Alternate',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
    // SNIP
    'LBL_CONTACT_HISTORY_SUBPANEL_TITLE' => 'Related Contacts\' Emails',
	'LBL_PRODUCTS_SERVICES_PURCHASED_SUBPANEL_TITLE' => 'Products and Services Purchased',
);
?>
