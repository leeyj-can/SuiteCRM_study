<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_MODULE_NAME' => '영업활동',
  'LBL_MODULE_TITLE' => 'Activities: Home',
  'LBL_SEARCH_FORM_TITLE' => 'Activities Search',
  'LBL_LIST_FORM_TITLE' => 'Activities List',
  'LBL_LIST_SUBJECT' => '제목',
  'LBL_LIST_CONTACT' => '거래담당자',
  'LBL_LIST_RELATED_TO' => '관련처',
  'LBL_LIST_DATE' => 'Date',
  'LBL_LIST_TIME' => '시작시간',
  'LBL_LIST_CLOSE' => '종료',
  'LBL_SUBJECT' => '제목:',
  'LBL_STATUS' => '상태:',
  'LBL_LOCATION' => '장소:',
  'LBL_DATE_TIME' => '시작일시:',
  'LBL_DATE' => '시작일:',
  'LBL_TIME' => '시작시간:',
  'LBL_DURATION' => '시간:',
  'LBL_DURATION_MINUTES' => '분:',
  'LBL_HOURS_MINS' => '(시간/분)',
  'LBL_CONTACT_NAME' => 'Contact Name: ',
  'LBL_MEETING' => '미팅:',
  'LBL_DESCRIPTION_INFORMATION' => '상세정보',
  'LBL_DESCRIPTION' => '상세:',
  'LBL_COLON' => ':',
  'LNK_NEW_CALL' => '콜스케쥴작성',
  'LNK_NEW_MEETING' => '미팅스케쥴작성',
  'LNK_NEW_TASK' => '타스크작성',
  'LNK_NEW_NOTE' => '노트 작성',
  'LNK_NEW_EMAIL' => 'Create Archived Email',
  'LNK_CALL_LIST' => '콜리스트',
  'LNK_MEETING_LIST' => '미팅리스트',
  'LNK_TASK_LIST' => '타스크리스트',
  'LNK_NOTE_LIST' => '노트',
  'LNK_EMAIL_LIST' => 'View Emails',
  'LBL_DELETE_ACTIVITY' => 'Are you sure you want to delete this activity?',
  'ERR_DELETE_RECORD' => 'You must specify a record number to delete the account.',
  'NTC_REMOVE_INVITEE' => 'Are you sure you want to remove this invitee from the meeting?',
  'LBL_INVITEE' => '참가자',
  'LBL_LIST_DIRECTION' => '방향',
  'LBL_DIRECTION' => '방향',
  'LNK_NEW_APPOINTMENT' => 'New Appointment',
  'LNK_VIEW_CALENDAR' => 'View Calendar',
  'LBL_OPEN_ACTIVITIES' => 'Open Activities',
  'LBL_HISTORY' => '이력',
  'LBL_UPCOMING' => 'My Upcoming Appointments',
  'LBL_TODAY' => 'through ',
  'LBL_NEW_TASK_BUTTON_TITLE' => '타스크작성',
  'LBL_NEW_TASK_BUTTON_KEY' => 'N',
  'LBL_NEW_TASK_BUTTON_LABEL' => '타스크작성',
  'LBL_SCHEDULE_MEETING_BUTTON_TITLE' => '미팅스케쥴작성',
  'LBL_SCHEDULE_MEETING_BUTTON_KEY' => 'M',
  'LBL_SCHEDULE_MEETING_BUTTON_LABEL' => '미팅스케쥴작성',
  'LBL_SCHEDULE_CALL_BUTTON_TITLE' => '콜스케쥴작성',
  'LBL_SCHEDULE_CALL_BUTTON_KEY' => 'C',
  'LBL_SCHEDULE_CALL_BUTTON_LABEL' => '콜스케쥴작성',
  'LBL_NEW_NOTE_BUTTON_TITLE' => '노트작성',
  'LBL_NEW_NOTE_BUTTON_KEY' => 'T',
  'LBL_NEW_NOTE_BUTTON_LABEL' => '노트작성',
  'LBL_TRACK_EMAIL_BUTTON_TITLE' => '메일작성',
  'LBL_TRACK_EMAIL_BUTTON_KEY' => 'K',
  'LBL_TRACK_EMAIL_BUTTON_LABEL' => '메일작성',
  'LBL_LIST_STATUS' => '상태',
  'LBL_LIST_DUE_DATE' => '종료일',
  'LBL_LIST_LAST_MODIFIED' => '최종변경일',
  'NTC_NONE_SCHEDULED' => 'None scheduled.',
  'appointment_filter_dom' => array(
  	 'today' => '오늘'
  	,'tomorrow' => '내일'
  	,'this Saturday' => '이번주'
  	,'next Saturday' => '다음주'
  	,'last this_month' => '이번달'
  	,'last next_month' => '다음달'
),
  'LNK_IMPORT_CALLS'=>'Import Calls',
  'LNK_IMPORT_MEETINGS'=>'Import Meetings',
  'LNK_IMPORT_TASKS'=>'Import Tasks',
  'LNK_IMPORT_NOTES'=>'노트읽어들이기',
  'NTC_NONE'=>'미사용',
  'LBL_ACCEPT_THIS'=>'허가?',
  'LBL_DEFAULT_SUBPANEL_TITLE' => 'Open Activities',
  'LBL_LIST_ASSIGNED_TO_NAME' => '할당유저',

	'LBL_ACCEPT' => '허가' /*for 508 compliance fix*/,
);


?>