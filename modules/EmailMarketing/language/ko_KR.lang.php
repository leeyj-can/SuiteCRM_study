<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_REPLY_ADDR' => '"Reply-to" Address: ',
  'LBL_REPLY_NAME' => '"Reply-to" Name: ',

  'LBL_MODULE_NAME' => 'E메일 마켓팅',
  'LBL_MODULE_TITLE' => 'E메일 마켓팅',
  'LBL_LIST_FORM_TITLE' => 'Email Marketing Campaigns',
  'LBL_NAME' => '명칭: ',
  'LBL_LIST_NAME' => '이름',
  'LBL_LIST_FROM_ADDR' => 'From Email',
  'LBL_LIST_DATE_START' => '시작일',
  'LBL_LIST_TEMPLATE_NAME' => 'Email Template',
  'LBL_LIST_STATUS' => '상태',
  'LBL_STATUS'	=>	'상태',
  'LBL_STATUS_TEXT'	=>	'상태:' ,
  'LBL_TEMPLATE_NAME'=>'Template Name',
  'LBL_DATE_ENTERED' => 'Date Entered',
  'LBL_DATE_MODIFIED' => '편집일',
  'LBL_MODIFIED' => '편집자:',
  'LBL_CREATED' => '등록자: ',
  'LBL_MESSAGE_FOR'	=> 'Send This Message To:',
  'LBL_MESSAGE_FOR_ID'	=> 'Message For',

  'LBL_FROM_NAME' => 'From Name: ',
  'LBL_FROM_ADDR' => 'From Email Address: ',
  'LBL_DATE_START' => '시작일',
  'LBL_TIME_START' => '시작시간',
  'LBL_START_DATE_TIME' => 'Start Date & Time: ',
  'LBL_TEMPLATE' => 'Email Template: ',

  'LBL_MODIFIED_BY' => '편집자:',
  'LBL_CREATED_BY' => '등록자: ',
  'LBL_DATE_CREATED' => 'Created date: ',
  'LBL_DATE_LAST_MODIFIED' => 'Modified date: ',

  'LNK_NEW_CAMPAIGN' => 'Create Campaign',
  'LNK_CAMPAIGN_LIST' => '캠페인',
  'LNK_NEW_PROSPECT_LIST' => '타켓리스트작성',
  'LNK_PROSPECT_LIST_LIST' => '타켓리스트',
  'LNK_NEW_PROSPECT' => '타켓작성',
  'LNK_PROSPECT_LIST' => '타켓',
  'LBL_DEFAULT_SUBPANEL_TITLE'=>'E메일 마켓팅',
  'LBL_CREATE_EMAIL_TEMPLATE'=>	'작성',
  'LBL_EDIT_EMAIL_TEMPLATE'=>	'편집',
  'LBL_FROM_MAILBOX'=>'From Mailbox',
  'LBL_FROM_MAILBOX_NAME'=>'Use Mailbox:',
  'LBL_PROSPECT_LIST_SUBPANEL_TITLE'=>'타켓리스트',
  'LBL_ALL_PROSPECT_LISTS'=>'Select to choose all Target List(s) in the Campaign.',
  'LBL_RELATED_PROSPECT_LISTS'=>'All Target List(s) related to this message.',
  'LBL_PROSPECT_LIST_NAME'=>'명칭:',
  'LBL_LIST_PROSPECT_LIST_NAME'=>'Targeted Lists',
  'LBL_MODULE_SEND_TEST'=>'Campaign: Send Test',
  'LBL_MODULE_SEND_EMAILS'=>'Campaign: Send Emails',
  'LBL_SCHEDULE_MESSAGE_TEST'=>'Please select the campaign messages that you would like to test:',
  'LBL_SCHEDULE_MESSAGE_EMAILS'=>'Please select the campaign messages that you would like to schedule for distribution on the specified start date and time:',
  'LBL_SCHEDULE_BUTTON_TITLE'=>'송신[Alt+S]',
  'LBL_SCHEDULE_BUTTON_LABEL'=>'송신[Alt+S]',
  'LBL_SCHEDULE_BUTTON_KEY'=>'T',



);
?>