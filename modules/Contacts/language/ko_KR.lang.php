<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

* Description:  Defines the English language pack for the base application.
* Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
* All Rights Reserved.
* Contributor(s): ______________________________________..
********************************************************************************/

$mod_strings = array (
    //DON'T CONVERT THESE THEY ARE MAPPINGS
    'db_last_name' => 'LBL_LIST_LAST_NAME',
    'db_first_name' => 'LBL_LIST_FIRST_NAME',
    'db_title' => 'LBL_LIST_TITLE',
    'db_email1' => 'LBL_LIST_EMAIL_ADDRESS',
    'db_email2' => 'LBL_LIST_OTHER_EMAIL_ADDRESS',
    //END DON'T CONVERT
    'ERR_DELETE_RECORD' => 'Specify the record number to delete the contact.',
    'LBL_ACCOUNT_ID' => '거래처ID:',
    'LBL_ACCOUNT_NAME' => '거래처명:',
    'LBL_CAMPAIGN'     => '캠페인:',
    'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'영업활동',
    'LBL_ADD_BUSINESSCARD' => '명함에서입력',
    'LBL_ADDMORE_BUSINESSCARD' => '다른명함에서입력',
    'LBL_ADDRESS_INFORMATION' => '주소정보',
    'LBL_ALT_ADDRESS_CITY' => '부가주소 시도구:',
    'LBL_ALT_ADDRESS_COUNTRY' => '부가주소 국가:',
    'LBL_ALT_ADDRESS_POSTALCODE' => '부가주소  우편번호:',
    'LBL_ALT_ADDRESS_STATE' => '부가주소 동읍면:',
    'LBL_ALT_ADDRESS_STREET_2' => '부가주소 주소 2:',
    'LBL_ALT_ADDRESS_STREET_3' => '부가주소 주소 3:',
    'LBL_ALT_ADDRESS_STREET' => '부가주소 주소 1:',
    'LBL_ALTERNATE_ADDRESS' => '기타주소:',
    'LBL_ALT_ADDRESS' => '연락주소:',
    'LBL_ANY_ADDRESS' => '주소:',
    'LBL_ANY_EMAIL' => 'E메일',
    'LBL_ANY_PHONE' => '전화:',
    'LBL_ASSIGNED_TO_NAME' => '할당유저:',
    'LBL_ASSIGNED_TO_ID' => '할당유저',
    'LBL_ASSISTANT_PHONE' => '보조자 전화:',
    'LBL_ASSISTANT' => '보조자:',
    'LBL_BIRTHDATE' => '생년월일:',
    'LBL_BUSINESSCARD' => '명함',
    'LBL_CITY' => '시도구:',
    'LBL_CAMPAIGN_ID' => 'Campaign ID',
    'LBL_CONTACT_INFORMATION' => '거래 담당자 정보',
    'LBL_CONTACT_NAME' => '거래담당자명:',
    'LBL_CONTACT_OPP_FORM_TITLE' => '거래담당자-안건:',
    'LBL_CONTACT_ROLE' => '역활:',
    'LBL_CONTACT' => '거래담당자:',
    'LBL_COUNTRY' => '국가:',
    'LBL_CREATED_ACCOUNT' => '새로운거래처가 작성되었습니다',
    'LBL_CREATED_CALL' => '새로운콜이 작성되었습니다',
    'LBL_CREATED_CONTACT' => '새로운 거래담당자가 작성되었습니다',
    'LBL_CREATED_MEETING' => '새로운 미팅약속이 작성되었습니다',
    'LBL_CREATED_OPPORTUNITY' =>'새로운 안건이 작성되었습니다',
    'LBL_DATE_MODIFIED' => '최종변경일:',
    'LBL_DEFAULT_SUBPANEL_TITLE' => '거래담당자',
    'LBL_DEPARTMENT' => '부문:',
    'LBL_DESCRIPTION_INFORMATION' => '상세 정보',
    'LBL_DESCRIPTION' => '상세:',
    'LBL_DIRECT_REPORTS_SUBPANEL_TITLE'=>'직속상사',
    'LBL_DO_NOT_CALL' => '전화불가:',
    'LBL_DUPLICATE' => '중복 가능성이있는 거래담당자',
    'LBL_EMAIL_ADDRESS' => 'E메일:',
    'LBL_EMAIL_OPT_OUT' => 'E메일 송신제외:',
    'LBL_EXISTING_ACCOUNT' => '기존 거래처를 사용',
    'LBL_EXISTING_CONTACT' => '기존 거래담당자를 사용',
    'LBL_EXISTING_OPPORTUNITY'=> '기존 안건정보를 사용',
    'LBL_FAX_PHONE' => '팩스:',
    'LBL_FIRST_NAME' => '이름(성):',
    'LBL_FULL_NAME' => '이름:',
    'LBL_HISTORY_SUBPANEL_TITLE'=>'이력',
    'LBL_HOME_PHONE' => '자택전화:',
    'LBL_ID' => 'ID:',
    'LBL_IMPORT_VCARD' => 'vCard에서작성',
    'LBL_VCARD' => 'vCard',
    'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new contact by importing a vCard from your file system.',
    'LBL_INVALID_EMAIL'=>'무효메일:',
    'LBL_INVITEE' => '직속상사',
    'LBL_LAST_NAME' => '이름(명):',
    'LBL_LEAD_SOURCE' => '리드 소스:',
    'LBL_LIST_ACCEPT_STATUS' => '허가상태',
    'LBL_LIST_ACCOUNT_NAME' => '거래처명',
    'LBL_LIST_CONTACT_NAME' => '거래담당자명',
    'LBL_LIST_CONTACT_ROLE' => '역활',
    'LBL_LIST_EMAIL_ADDRESS' => 'E메일',
    'LBL_LIST_FIRST_NAME' => '이름(성)',
    'LBL_LIST_FORM_TITLE' => '거래담당 리스트',
    'LBL_VIEW_FORM_TITLE' => '거래담당 표시',
    'LBL_LIST_LAST_NAME' => '이름(명)',
    'LBL_LIST_NAME' => '이름',
    'LBL_LIST_OTHER_EMAIL_ADDRESS' => '다른 E메일',
    'LBL_LIST_PHONE' => '직장 전화',
    'LBL_LIST_TITLE' => '직위',
    'LBL_MOBILE_PHONE' => '휴대폰:',
    'LBL_MODIFIED' => '변경한 유저ID:',
    'LBL_MODULE_NAME' => '거래담당자',
    'LBL_MODULE_TITLE' => '거래담당자: 홈',
    'LBL_NAME' => '이름:',
    'LBL_NEW_FORM_TITLE' => '신규거래담당자',
    'LBL_NEW_PORTAL_PASSWORD' => '새로운 포탈 패스워드:',
    'LBL_NOTE_SUBJECT' =>'노트 제목',
    'LBL_OFFICE_PHONE' => '지장전화:',
    'LBL_OPP_NAME' => '안건명:',
    'LBL_OPPORTUNITY_ROLE_ID'=>'안건 역활 ID:',
    'LBL_OPPORTUNITY_ROLE'=>'안건 역활',
    'LBL_OTHER_EMAIL_ADDRESS' => '다른 E메일:',
    'LBL_OTHER_PHONE' => '다른 전화:',
    'LBL_PHONE' => '전화:',
    'LBL_PORTAL_ACTIVE' => '포탈 활동:',
    'LBL_PORTAL_APP'=>'포탈 애플리케이션:',
    'LBL_PORTAL_INFORMATION' => '포탈정보',
    'LBL_PORTAL_NAME' => '포탈명:',
    'LBL_PORTAL_PASSWORD_ISSET' => '포탈 패스워드 설정:',
    'LBL_STREET' => 'Street',
    'LBL_POSTAL_CODE' => '우편번호:',
    'LBL_PRIMARY_ADDRESS_CITY' => '메인주소 시구군:',
    'LBL_PRIMARY_ADDRESS_COUNTRY' => '메인주소 국가:',
    'LBL_PRIMARY_ADDRESS_POSTALCODE' => '메인주소 우편번호:',
    'LBL_PRIMARY_ADDRESS_STATE' => '메인주소 읍면동:',
    'LBL_PRIMARY_ADDRESS_STREET_2' => '메인주소 주소2:',
    'LBL_PRIMARY_ADDRESS_STREET_3' => '메인주소 주소3:',
    'LBL_PRIMARY_ADDRESS_STREET' => '메인주소 주소:',
    'LBL_PRIMARY_ADDRESS' => '메인주소:',
    'LBL_PRODUCTS_TITLE'=>'제품',
    'LBL_RELATED_CONTACTS_TITLE'=>'관련거래담당자',
    'LBL_REPORTS_TO_ID'=>'상사ID:',
    'LBL_REPORTS_TO' => '상사:',
    'LBL_RESOURCE_NAME' => 'Resource Name',
    'LBL_SALUTATION' => '경칭:',
    'LBL_SAVE_CONTACT' => '거래 담당자 저장',
    'LBL_SEARCH_FORM_TITLE' => '거래담당자 검색',
    'LBL_SELECT_CHECKED_BUTTON_LABEL' => '체크된 거래담당자 선택',
    'LBL_SELECT_CHECKED_BUTTON_TITLE' => '체크된 거래담당자 선택',
    'LBL_STATE' => '읍면동:',
    'LBL_SYNC_CONTACT' => '아웃룩하고동기:',
    'LBL_PROSPECT_LIST' => 'Prospect List',
    'LBL_TITLE' => '직위:',
    'LNK_CONTACT_LIST' => '거래담당자리스트',
    'LNK_IMPORT_VCARD' => 'vCard에서작성',
    'LNK_NEW_ACCOUNT' => '거래처 작성',
    'LNK_NEW_APPOINTMENT' => '약속 작성',
    'LNK_NEW_CALL' => '콜스케쥴작성',
    'LNK_NEW_CASE' => '사례작성',
    'LNK_NEW_CONTACT' => '거래담당자작성',
    'LNK_NEW_EMAIL' => 'E메일작성',
    'LNK_NEW_MEETING' => '미팅스케쥴작성',
    'LNK_NEW_NOTE' => '노트작성',
    'LNK_NEW_OPPORTUNITY' => '안건작성',
    'LNK_NEW_TASK' => '타스크작성',
    'LNK_SELECT_ACCOUNT' => "거래처 선택",
	'MSG_DUPLICATE' => 'Creating this contact may potentialy create a duplicate contact. You may either select a contact from the list below or you may click on Create Contact to continue creating a new contact with the previously entered data.',
	'MSG_SHOW_DUPLICATES' => 'Creating this contact may potentialy create a duplicate contact. You may either click on Create Contact to continue creating this new contact with the previously entered data or you may click Cancel.',
    'NTC_COPY_ALTERNATE_ADDRESS' => 'Copy alternate address to primary address',
    'NTC_COPY_PRIMARY_ADDRESS' => 'Copy primary address to alternate address',
    'NTC_DELETE_CONFIRMATION' => 'Are you sure you want to delete this record?',
    'NTC_OPPORTUNITY_REQUIRES_ACCOUNT' => 'Creating an opportunity requires an account.\n Please either create a new one or select an existing one.',
    'NTC_REMOVE_CONFIRMATION' => 'Are you sure you want to remove this contact from this case?',
    'NTC_REMOVE_DIRECT_REPORT_CONFIRMATION' => 'Are you sure you want to remove this record as a direct report?',

	'LBL_USER_PASSWORD' => '패스워드:',

	'LBL_LEADS_SUBPANEL_TITLE' => '리드',
	'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => '안건',
	'LBL_DOCUMENTS_SUBPANEL_TITLE' => '자료관리',
	'LBL_COPY_ADDRESS_CHECKED_PRIMARY' => 'Copy to Primary Address',
	'LBL_COPY_ADDRESS_CHECKED_ALT' => 'Copy to Other Address',

	'LBL_CASES_SUBPANEL_TITLE' => '사례',
	'LBL_BUGS_SUBPANEL_TITLE' => '결함',
	'LBL_PROJECTS_SUBPANEL_TITLE' => '프로젝트',
    'LBL_PROJECTS_RESOURCES' => 'Projects Resources',
	'LBL_TARGET_OF_CAMPAIGNS' => '캠페인 (타켓의) :',
	'LBL_CAMPAIGNS'	=>	'캠페인',
	'LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'=>'캠페인 로그',
	'LBL_LIST_CITY' => '시구군',
	'LBL_LIST_STATE' => '읍면동',
	'LBL_HOMEPAGE_TITLE' => 'My 거래담당자',
    'LBL_OPPORTUNITIES' => '안건',

	'LBL_CHECKOUT_DATE'=>'Checkout Date',
    'LBL_CONTACTS_SUBPANEL_TITLE' => '거래담당자',
    'LBL_PROJECT_SUBPANEL_TITLE' => '프로젝트',
    'LBL_CAMPAIGNS_SUBPANEL_TITLE' => '캠페인',
    'LNK_IMPORT_CONTACTS' => 'Import Contacts',

    //For export labels
    'LBL_PHONE_HOME' => 'Phone Home',
    'LBL_PHONE_MOBILE' => '모바일:',
    'LBL_PHONE_WORK' => 'Phone Work',
    'LBL_PHONE_OTHER' => 'Phone Other',
    'LBL_PHONE_FAX' => '팩스:',

    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Assigned User ID',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Modified By ID',
    'LBL_EXPORT_CREATED_BY' => 'Created By ID',
    'LBL_EXPORT_PHONE_HOME' => '자택전화:',
    'LBL_EXPORT_PHONE_MOBILE' => 'Mobile Phone',
    // SNIP
    'LBL_CONTACT_HISTORY_SUBPANEL_TITLE' => 'Related Contacts\' Emails',
    'LBL_USER_SYNC' => 'User Sync'
)
?>
