<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_MODULE_NAME' => '결함정보',
  'LBL_MODULE_TITLE' => '결함정보: 홈',
  'LBL_MODULE_ID' => '결함정보',
  'LBL_SEARCH_FORM_TITLE' => '결함정보 검색',
  'LBL_LIST_FORM_TITLE' => '결함정보 리스',
  'LBL_NEW_FORM_TITLE' => '결함정보 작성',
  'LBL_CONTACT_BUG_TITLE' => '거래담당자-결함정보:',
  'LBL_SUBJECT' => '제목:',
  'LBL_BUG' => '결함정보:',
  'LBL_BUG_NUMBER' => '결함정보 넘버:',
  'LBL_NUMBER' => '넘버:',
  'LBL_STATUS' => '상태:',
  'LBL_PRIORITY' => '우선순위:',
  'LBL_DESCRIPTION' => '상세:',
  'LBL_CONTACT_NAME' => '거래담당자명:',
  'LBL_BUG_SUBJECT' => '결함 제목:',
  'LBL_CONTACT_ROLE' => '역활:',
  'LBL_LIST_NUMBER' => '넘버.',
  'LBL_LIST_SUBJECT' => '제목',
  'LBL_LIST_STATUS' => '상태',
  'LBL_LIST_PRIORITY' => '우선순위',
  'LBL_LIST_RELEASE' => '릴리즈',
  'LBL_LIST_RESOLUTION' => '리솔루션',
  'LBL_LIST_LAST_MODIFIED' => '최종병경일',
  'LBL_INVITEE' => '거래담당자',
  'LBL_TYPE' => '종류:',
  'LBL_LIST_TYPE' => '종류',
  'LBL_RESOLUTION' => '리솔루션:',
  'LBL_RELEASE' => '릴리즈:',
  'LNK_NEW_BUG' => '결함정보작성',
  'LNK_BUG_LIST' => '결함정보리스트',
  'NTC_REMOVE_INVITEE' => 'Are you sure you want to remove this contact from the bug?',
  'NTC_REMOVE_ACCOUNT_CONFIRMATION' => 'Are you sure you want to remove this bug from this account?',
  'ERR_DELETE_RECORD' => 'You must specify a record number in order to delete the bug.',
  'LBL_LIST_MY_BUGS' => 'My 할당 결함정보',
  'LNK_IMPORT_BUGS' => 'Import Bugs',
  'LBL_FOUND_IN_RELEASE' => '릴리즈 검색:',
  'LBL_FIXED_IN_RELEASE' => '수정완료 릴리즈:',
  'LBL_LIST_FIXED_IN_RELEASE' => '수정완료 릴리즈',
  'LBL_WORK_LOG' => '워크로그:',
  'LBL_SOURCE' => '소스:',
  'LBL_PRODUCT_CATEGORY' => '카테고리:',

  'LBL_CREATED_BY' => '등록자:',
  'LBL_DATE_CREATED' => '등록일:',
  'LBL_MODIFIED_BY' => '최종편집일:',
  'LBL_DATE_LAST_MODIFIED' => '최종편집일:',

  'LBL_LIST_EMAIL_ADDRESS' => 'E메일 주소',
  'LBL_LIST_CONTACT_NAME' => '거래담당자명',
  'LBL_LIST_ACCOUNT_NAME' => '거래처명',
  'LBL_LIST_PHONE' => '전화',
  'NTC_DELETE_CONFIRMATION' => 'Are you sure you want to remove this contact from this bug?',

  'LBL_DEFAULT_SUBPANEL_TITLE' => '결함 정보',
  'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'영업활동',
  'LBL_HISTORY_SUBPANEL_TITLE'=>'이력',
  'LBL_CONTACTS_SUBPANEL_TITLE' => '거래담당자',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => '거래처',
  'LBL_CASES_SUBPANEL_TITLE' => '사례',
  'LBL_PROJECTS_SUBPANEL_TITLE' => '프로젝트',
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => '자료관리',
  'LBL_SYSTEM_ID' => '시스템ID',
  'LBL_LIST_ASSIGNED_TO_NAME' => '할당유저',
	'LBL_ASSIGNED_TO_NAME' => '할당유저',

	'LBL_BUG_INFORMATION' => '리드 정보',

    //For export labels
	'LBL_FOUND_IN_RELEASE_NAME' => 'Found In Release Name',
    'LBL_PORTAL_VIEWABLE' => 'Portal Viewable',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Assigned User Name',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Assigned User ID',
    'LBL_EXPORT_FIXED_IN_RELEASE_NAMR' => 'Fixed in Release Name',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Modified By ID',
    'LBL_EXPORT_CREATED_BY' => 'Created By ID',


  );
?>
