<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

$mod_strings = array(
'LBL_SALUTATION'=>'경칭',
'LBL_NAME'=>'이름',
'LBL_FIRST_NAME'=>'이름(명):',
'LBL_LAST_NAME'=>'이름(명)', 
'LBL_TITLE'=>'직위:',
'LBL_DEPARTMENT'=>'부서',
'LBL_DO_NOT_CALL'=>'Do Not Call',
'LBL_HOME_PHONE'=>'자택전화:',
'LBL_MOBILE_PHONE'=>'Mobile Phone',
'LBL_OFFICE_PHONE'=>'전화:',
'LBL_OTHER_PHONE'=>'전화:',
'LBL_FAX_PHONE'=>'팩스:',
'LBL_EMAIL_ADDRESS'=>'메일주소',
'LBL_PRIMARY_ADDRESS'=>'주소:', 
'LBL_PRIMARY_ADDRESS_STREET'=>'주소:',
'LBL_PRIMARY_ADDRESS_STREET_2' => '메인주소 주소2:',
'LBL_PRIMARY_ADDRESS_STREET_3' => '메인주소 주소3:',
'LBL_PRIMARY_ADDRESS_CITY'=>'주소도시',
'LBL_PRIMARY_ADDRESS_STATE'=>'Primary State',
'LBL_PRIMARY_ADDRESS_POSTALCODE'=>'Primary Postal Code',
'LBL_PRIMARY_ADDRESS_COUNTRY' => '메인주소 국가:',
'LBL_ALT_ADDRESS'=>'Alternate Address', 
'LBL_ALT_ADDRESS_STREET'=>'Alternate Address',
'LBL_ALT_ADDRESS_STREET_2' => '부가주소 주소 2:',
'LBL_ALT_ADDRESS_STREET_3' => '부가주소 주소 3:',
'LBL_ALT_ADDRESS_CITY'=>'Alternate City',
'LBL_ALT_ADDRESS_STATE'=>'Alternate State',
'LBL_ALT_ADDRESS_POSTALCODE'=>'Alternate Postal Code',
'LBL_ALT_ADDRESS_COUNTRY'=>'Alternate Country',
'LBL_STREET'=>'다른주소',
'LBL_PRIMARY_STREET'=>'주소:',
'LBL_ALT_STREET'=>'다른주소',
'LBL_CITY'=>'시구군:',
'LBL_STATE'=>'읍면동:',
'LBL_POSTALCODE'=>'우편번호:',
'LBL_POSTAL_CODE'=>'우편번호:',
'LBL_COUNTRY'=>'나라:',
'LBL_CONTACT_INFORMATION'=>'연락처안내',
'LBL_ADDRESS_INFORMATION'=>'Address(es)',
'LBL_ASSIGNED_TO_NAME'=>'유저',
'LBL_OTHER_EMAIL_ADDRESS' => '연락E메일:',
'LBL_ASSISTANT'=>'보조',
'LBL_ASSISTANT_PHONE'=>'보조전화',
'LBL_WORK_PHONE'=>'전화:',
'LNK_IMPORT_VCARD' => 'Create From vCard',
'LBL_ANY_EMAIL' => '부사용 메일:',
'LBL_EMAIL_NON_PRIMARY' => '비주요메일',
'LBL_PHOTO' => '사진',

'LBL_EDIT_BUTTON' => '편집',
'LBL_REMOVE' => '삭제',

);