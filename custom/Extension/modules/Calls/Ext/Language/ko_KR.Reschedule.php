<?php
$array = array(
		'LBL_RESCHEDULE' => 'Reschedule',
		'LBL_RESCHEDULE_COUNT' => 'Call Attempts',
		'LBL_RESCHEDULE_DATE' => 'Date',
		'LBL_RESCHEDULE_REASON' => '원인',
		'LBL_RESCHEDULE_ERROR1' => 'Please select a valid date',
		'LBL_RESCHEDULE_ERROR2' => 'Please select a reason',
		'LBL_RESCHEDULE_PANEL' => 'Reschedule',
		'LBL_RESCHEDULE_HISTORY' => 'Call Attempt History',
	);

$mod_strings = array_merge($mod_strings, $array);
?>