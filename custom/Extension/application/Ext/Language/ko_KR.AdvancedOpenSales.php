<?php 
/**
 * Products, Quotations & Invoices modules.
 * Extensions to SugarCRM
 * @package Advanced OpenSales for SugarCRM
 * @subpackage Products
 * @copyright SalesAgility Ltd http://www.salesagility.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author Salesagility Ltd <support@salesagility.com>
 */

$app_list_strings['moduleList']['AOS_Contracts'] = 'Contracts';
$app_list_strings['moduleList']['AOS_Invoices'] = 'Invoices';
$app_list_strings['moduleList']['AOS_PDF_Templates'] = 'PDF Templates';
$app_list_strings['moduleList']['AOS_Product_Categories'] = 'Product Categories';
$app_list_strings['moduleList']['AOS_Products'] = '제품';
$app_list_strings['moduleList']['AOS_Products_Quotes'] = 'Line Items';
$app_list_strings['moduleList']['AOS_Line_Item_Groups'] = 'Line Item Groups';
$app_list_strings['moduleList']['AOS_Quotes'] = '견적';
$app_list_strings['aos_quotes_type_dom'][''] = '';
$app_list_strings['aos_quotes_type_dom']['Analyst'] = '아날리스트';
$app_list_strings['aos_quotes_type_dom']['Competitor'] = '경쟁업체';
$app_list_strings['aos_quotes_type_dom']['Customer'] = '거래처';
$app_list_strings['aos_quotes_type_dom']['Integrator'] = '시스템설계업체';
$app_list_strings['aos_quotes_type_dom']['Investor'] = '투자업체';
$app_list_strings['aos_quotes_type_dom']['Partner'] = '파트너';
$app_list_strings['aos_quotes_type_dom']['Press'] = '프레스';
$app_list_strings['aos_quotes_type_dom']['Prospect'] = '잠재거래처';
$app_list_strings['aos_quotes_type_dom']['Reseller'] = '리세일러';
$app_list_strings['aos_quotes_type_dom']['Other'] = '기타:';
$app_list_strings['template_ddown_c_list'][''] = '';
$app_list_strings['quote_stage_dom']['Draft'] = '드래프트';
$app_list_strings['quote_stage_dom']['Negotiation'] = '교섭중';
$app_list_strings['quote_stage_dom']['Delivered'] = '출하완료';
$app_list_strings['quote_stage_dom']['On Hold'] = '출하홀딩';
$app_list_strings['quote_stage_dom']['Confirmed'] = '확인완료';
$app_list_strings['quote_stage_dom']['Closed Accepted'] = '수령완료';
$app_list_strings['quote_stage_dom']['Closed Lost'] = '수주실패';
$app_list_strings['quote_stage_dom']['Closed Dead'] = '무효처리완료';
$app_list_strings['quote_term_dom']['Net 15'] = 'Nett 15';
$app_list_strings['quote_term_dom']['Net 30'] = 'Nett 30';
$app_list_strings['quote_term_dom'][''] = '';
$app_list_strings['approval_status_dom']['Approved'] = 'Approved';
$app_list_strings['approval_status_dom']['Not Approved'] = 'Not Approved';
$app_list_strings['approval_status_dom'][''] = '';
$app_list_strings['vat_list']['0.0'] = '0%';
$app_list_strings['vat_list']['5.0'] = '5%';
$app_list_strings['vat_list']['7.5'] = '7.5%';
$app_list_strings['vat_list']['17.5'] = '17.5%';
$app_list_strings['vat_list']['20.0'] = '20%';
$app_list_strings['discount_list']['Percentage'] = 'Pct';
$app_list_strings['discount_list']['Amount'] = 'Amt';
$app_list_strings['aos_invoices_type_dom'][''] = '';
$app_list_strings['aos_invoices_type_dom']['Analyst'] = '아날리스트';
$app_list_strings['aos_invoices_type_dom']['Competitor'] = '경쟁업체';
$app_list_strings['aos_invoices_type_dom']['Customer'] = '거래처';
$app_list_strings['aos_invoices_type_dom']['Integrator'] = '시스템설계업체';
$app_list_strings['aos_invoices_type_dom']['Investor'] = '투자업체';
$app_list_strings['aos_invoices_type_dom']['Partner'] = '파트너';
$app_list_strings['aos_invoices_type_dom']['Press'] = '프레스';
$app_list_strings['aos_invoices_type_dom']['Prospect'] = '잠재거래처';
$app_list_strings['aos_invoices_type_dom']['Reseller'] = '리세일러';
$app_list_strings['aos_invoices_type_dom']['Other'] = '기타:';
$app_list_strings['invoice_status_dom']['Paid'] = 'Paid';
$app_list_strings['invoice_status_dom']['Unpaid'] = 'Unpaid';
$app_list_strings['invoice_status_dom']['Cancelled'] = 'Cancelled';
$app_list_strings['invoice_status_dom'][''] = '';
$app_list_strings['quote_invoice_status_dom']['Not Invoiced'] = 'Not Invoiced';
$app_list_strings['quote_invoice_status_dom']['Invoiced'] = 'Invoiced';
$app_list_strings['product_code_dom']['XXXX'] = 'XXXX';
$app_list_strings['product_code_dom']['YYYY'] = 'YYYY';
$app_list_strings['product_category_dom']['Laptops'] = 'Laptops';
$app_list_strings['product_category_dom']['Desktops'] = 'Desktops';
$app_list_strings['product_category_dom'][''] = '';
$app_list_strings['product_type_dom']['Good'] = 'Good';
$app_list_strings['product_type_dom']['Service'] = 'Service';
$app_list_strings['product_quote_parent_type_dom']['AOS_Quotes'] = '견적';
$app_list_strings['product_quote_parent_type_dom']['AOS_Invoices'] = 'Invoices';
$app_list_strings['product_quote_parent_type_dom']['AOS_Contracts'] = 'Contracts';
$app_list_strings['pdf_template_type_dom']['AOS_Quotes'] = '견적';
$app_list_strings['pdf_template_type_dom']['AOS_Invoices'] = 'Invoices';
$app_list_strings['pdf_template_type_dom']['AOS_Contracts'] = 'Contracts';
$app_list_strings['pdf_template_type_dom']['Accounts'] = '거래처';
$app_list_strings['pdf_template_type_dom']['Contacts'] = '거래담당자';
$app_list_strings['pdf_template_type_dom']['Leads'] = '리드';
$app_list_strings['pdf_template_sample_dom'][''] = '';
$app_list_strings['contract_status_list']['Not Started'] = '미개시';
$app_list_strings['contract_status_list']['In Progress'] = '진행중';
$app_list_strings['contract_status_list']['Signed'] = 'Signed';
$app_list_strings['contract_type_list']['Type'] = '종류';
$app_strings['LBL_GENERATE_LETTER'] = 'Generate Letter';
$app_strings['LBL_SELECT_TEMPLATE'] = 'Please Select a Template';
$app_strings['LBL_NO_TEMPLATE'] = 'ERROR\nNo templates found.\nPlease go to the PDF templates module and create one';

